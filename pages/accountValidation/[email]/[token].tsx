import { withApollo } from 'hoc/withApollo.jsx';
import AppLayout from 'containers/layouts/AppLayout';
import { useSessionState } from 'context/session/session';
import { Avatar, Grid, Stack, Typography } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { useRouter } from 'next/router';
import { useCallback, useEffect } from 'react';
import gql from 'graphql-tag';
import { useMutation, useQuery } from '@apollo/client';
import useGraphQLErrorDisplay from 'hooks/useGraphQLErrorDisplay';
import UserPasswordForm, { UserPasswordFields } from 'containers/forms/UserPasswordForm';
import { SubmitHandler } from 'react-hook-form';

const IS_ACCOUNT_VALIDATED = gql`
  query isAccountValidated($email: String!, $token: String!) {
    isAccountValidated(email: $email, token: $token)
  }
`;

const VALIDATE_ACCOUNT = gql`
  mutation validateAccount($email: String!, $token: String!, $password: String!) {
    validateAccount(email: $email, token: $token, password: $password)
  }
`;

const AccountValidation = () => {
  const user = useSessionState();

  const router = useRouter();
  const { email, token } = router.query;

  const { data: checkData, error: getError } = useQuery(IS_ACCOUNT_VALIDATED, { variables: { email, token } });
  const [validateAccount, { data, error }] = useMutation(VALIDATE_ACCOUNT);

  useGraphQLErrorDisplay(getError);
  useGraphQLErrorDisplay(error);

  useEffect(() => {
    if (data && data.validateAccount) {
      router.push('/signin?from=accountValidation');
    }
  }, [data]);

  const onSubmit: SubmitHandler<UserPasswordFields> = useCallback(
    ({ password }) => {
      validateAccount({
        variables: {
          email,
          token,
          password,
        },
      }).catch((err) => {});
    },
    [email, token],
  );

  const isAccountValidated = user || checkData?.isAccountValidated === true;

  return (
    <AppLayout>
      <Grid container justifyContent="center">
        <Grid item xs={10} md={4} marginTop={6} marginBottom={6}>
          <Stack direction="column" spacing={1} alignItems="center" marginBottom={3}>
            <Avatar sx={{ backgroundColor: 'secondary.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Validation du compte
            </Typography>
            {isAccountValidated && (
              <Typography>
                Vous avez déjà validé votre compte
              </Typography>
            )}
            {!isAccountValidated && (
              <Typography>
                Afin de vous connecter au site avec votre email {email}, vous devez tout d'abord définir un mot de passe
                pour votre compte.
              </Typography>
            )}
          </Stack>
          {!isAccountValidated && <UserPasswordForm submitLabel="Valider" onSubmit={onSubmit} />}
        </Grid>
      </Grid>
    </AppLayout>
  );
};

export default withApollo()(AccountValidation);
