import React, { useState, useRef, useMemo, useCallback } from 'react';
import AppLayout from 'containers/layouts/AppLayout';
import {
  Container,
  Grid,
  Typography,
  useTheme,
  Stack,
  Link,
  Box,
  ToggleButtonGroup,
  ToggleButton,
} from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { withApollo } from 'hoc/withApollo.jsx';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/client';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import TelegramIcon from '@mui/icons-material/Telegram';
import EmailIcon from '@mui/icons-material/Email';
import { styled } from '@mui/material/styles';
import EventCard from 'components/cards/EventCard';
import ActionCard from 'components/cards/ActionCard';
import ImagesList from 'components/ImagesList';
import RichTextContent from 'components/RichTextContent';
import StageVolunteerModal from 'components/modals/StageVolunteerModal';

import {
  FacebookShareButton,
  TwitterShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  EmailShareButton,
} from 'react-share';
import Head from 'next/head';
import Fab from '@mui/material/Fab';
import EditIcon from '@mui/icons-material/Edit';
import Image from 'next/image';
import moment from 'moment';
import dynamic from 'next/dynamic';
import { useSessionState } from 'context/session/session';
import { getImageUrl, urlWithHttpsdefault, rruleToText } from 'utils/utils';
import Calendar from 'components/Calendar';

const MarkerClusterWithNoSSR = dynamic(() => import('components/map/MarkerCluster'), {
  ssr: false,
});

const GET_ARTICLES = gql`
  query articles($stageId: String) {
    articles(stageId: $stageId) {
      id
      label
      shortDescription
      createdAt
      pictures {
        id
        label
        originalPicturePath
        originalPictureFilename
        position
        main
      }
    }
  }
`;

const GET_EVENTS = gql`
  query events($stageId: String) {
    events(stageId: $stageId) {
      id
      label
      shortDescription
      description
      startedAt
      endedAt
      lat
      lng
      published
      pictures {
        id
        label
        originalPicturePath
        originalPictureFilename
        position
      }
      address
      city
      category
    }
  }
`;

const GET_STAGE_SSR = `
query stage($name: String) {
  stage(name: $name) {
    id
    name
    startedAt
    endedAt
    address
    lat
    lng
    address
    city
    email
    phone
    website
    volunteerAction
    volunteerForm
    description
    updatedAt
    shortDescription
    socialNetwork
    activity
    referents {
      id
      surname
      lastname
    }
    isValidated
    pictures {
      id
      label
      originalPicturePath
      originalPictureFilename
      position
      main
      partner
    }
  }
}
`;

const Banner = styled(Container)<{ url: string }>(({ theme, url }) => ({
  marginTop: theme.spacing(2),
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  height: '24em',
  backgroundImage: `url("${url}")`,
}));

const StageContent = styled(Container)(({ theme }) => ({
  backgroundColor: 'white',
  borderRadius: '0.5em',
  marginTop: -53,
  boxShadow: '0px 0px 38px -14px rgba(0, 0, 0, 0.46)',
  [theme.breakpoints.up('sm')]: {
    padding: '5em',
  },
  [theme.breakpoints.down('md')]: {
    paddingTop: 16,
  },
}));

const InfoBoxItem: React.FC<{ icon: string; iconAlt: string; divider?: boolean }> = ({
  icon,
  iconAlt,
  children,
  divider = true,
}) => (
  <>
    <Grid item xs={3} textAlign="right" padding={2}>
      <Image src={icon} width="20" height="25" objectFit="contain" alt={iconAlt} />
    </Grid>
    <Grid item xs={9} padding={2} paddingLeft={0}>
      {children}
    </Grid>
    {divider && <Grid item xs={12} sx={{ borderBottom: 'dashed 1px' }} />}
  </>
);

const Stage = ({ initialData }) => {
  const mapRef = useRef();

  const [currentLocationWindows] = useState(globalThis?.location);

  const [selectedButton, setSelectedButton] = useState<'list' | 'map' | 'calendar'>('list');

  const theme = useTheme();

  const [volunteerModalOpen, setVolunteerModalOpen] = useState(false);

  const isMobile = useMediaQuery(theme.breakpoints.down('md'));
  const MapWithNoSSR = dynamic(() => import('components/map/Map'), {
    ssr: false,
  });

  const MarkerEventWithNoSSR = dynamic(() => import('components/map/EventMarker'), {
    ssr: false,
  });
  const { data } = initialData;

  const { data: dataArticles } = useQuery(GET_ARTICLES, {
    variables: {
      stageId: `${data && data.stage.id}`,
    },
  });

  const { data: dataEvents } = useQuery(GET_EVENTS, {
    variables: {
      stageId: `${data && data.stage.id}`,
    },
  });

  const bannerUrl = useMemo(() => {
    return (data?.stage?.pictures || []).filter((picture) => picture.main).length >= 1
      ? data.stage.pictures.filter((picture) => picture.main)[0].originalPicturePath
      : null;
  }, [data]);

  const user = useSessionState();

  const canEdit = useCallback(() => {
    return (
      user && (user.role === 'admin' || (data?.stage?.referents || []).some((referent) => referent.id === user.id))
    );
  }, [data, user]);

  const events = useMemo(
    () =>
      (dataEvents?.events || []).map((evt) => {
        return {
          startDate: new Date(parseInt(evt.startedAt)),
          endDate: new Date(parseInt(evt.endedAt)),
          title: evt.label,
          id: evt.id,
          location: evt.city ? [evt.address, evt.city].join(', ') : '',
          category: evt.category,
          backgroundColor:
            evt.entries && evt.entries.length > 0 && evt.entries[0].parentEntry
              ? evt.entries[0].parentEntry.color
              : 'blue',
        };
      }),
    [dataEvents],
  );

  const formatDate = (timestamp: string) => {
    const date = new Date(parseInt(timestamp, 10));
    return new Intl.DateTimeFormat('fr-FR', {
      month: 'numeric',
      day: 'numeric',
      ...(date.getHours() === 0 && date.getMinutes() === 0
        ? {}
        : {
            hour: 'numeric',
            minute: 'numeric',
          }),
    }).format(date);
  };

  return (
    <AppLayout>
      <Head>
        <title>Étape de {data?.stage.city} - Tour Alternatiba 2024</title>
        <meta property="og:title" content={`Étape de ${data?.stage.city} -  Tour Alternatiba 2024`} />
        <meta property="og:description" content={data?.stage.shortDescription} />
        <meta name="twitter:title" content={`Étape de ${data?.stage.city} -  Tour Alternatiba 2024`} />
        <meta name="twitter:description" content={data?.stage.shortDescription} />
      </Head>
      <Box sx={{ mt: 6 }}>
        <Typography variant="h1" sx={{ display: 'none' }}>
          {data?.stage.name}
        </Typography>

        {bannerUrl && <Banner url={getImageUrl(bannerUrl) || ''} />}

        <StageContent>
          {isMobile && (
            <Typography variant="h1" textAlign="center" mb={3}>
              {data?.stage?.name}
            </Typography>
          )}

          <Grid container sx={{ marginBottom: 6 }} spacing={4}>
            <Grid item md={5} sm={10}>
              <Grid container sx={{ backgroundColor: '#ededf5', borderRadius: '5px' }}>
                <InfoBoxItem icon={'/icons/clock.svg'} iconAlt={'Horaire'}>
                  {data && (
                    <>
                      <Typography color="primary">Date de début</Typography>
                      <Typography>
                        <strong>{formatDate(data.stage.startedAt)}</strong>
                      </Typography>
                      <Typography color="primary">Date de fin</Typography>
                      <Typography>
                        <strong>{formatDate(data.stage.endedAt)}</strong>
                      </Typography>
                    </>
                  )}
                </InfoBoxItem>

                {data?.stage.email && (
                  <InfoBoxItem icon={'/icons/email.svg'} iconAlt={'Email'}>
                    <Typography color="primary">Contact des référents de l'étape</Typography>
                    <Typography>
                      <strong>
                        <Link
                          color="common.black"
                          href={`mailto:${data.stage.email}?subject=Je souhaite rejoindre l'orga de l'étape de ${data.stage.name}`}
                        >
                          {data.stage.email}
                        </Link>
                      </strong>
                    </Typography>
                  </InfoBoxItem>
                )}

                <InfoBoxItem icon={'/icons/social.svg'} iconAlt={'Réseaux sociaux'}>
                  <Typography color="primary">Partager la page sur les réseaux</Typography>
                  <Stack direction="row" spacing={1}>
                    <FacebookShareButton url={`${currentLocationWindows}`}>
                      <FacebookIcon />
                    </FacebookShareButton>
                    <TwitterShareButton url={`${currentLocationWindows}`}>
                      <TwitterIcon />
                    </TwitterShareButton>
                    <WhatsappShareButton url={`${currentLocationWindows}`}>
                      <WhatsAppIcon />
                    </WhatsappShareButton>
                    <TelegramShareButton url={`${currentLocationWindows}`}>
                      <TelegramIcon />
                    </TelegramShareButton>
                    <EmailShareButton url={`${currentLocationWindows}`}>
                      <EmailIcon />
                    </EmailShareButton>
                  </Stack>
                </InfoBoxItem>

                {data?.stage.website && (
                  <InfoBoxItem icon={'/icons/web_site.svg'} iconAlt={'Site Web'} divider={false}>
                    <Typography color="primary">Site web</Typography>
                    <Typography>
                      <strong>
                        <Link href={urlWithHttpsdefault(data.stage.website)} target="_blank" rel="noreferrer">
                          {data.stage.website}
                        </Link>
                      </strong>
                    </Typography>
                  </InfoBoxItem>
                )}
              </Grid>
            </Grid>

            <Grid item md={7} sm={10} textAlign="justify">
              {!isMobile && (
                <Typography variant="h1" textAlign="center" mb={6}>
                  {data?.stage?.name}
                </Typography>
              )}
              <div>{data && <RichTextContent content={data.stage.description} />}</div>
            </Grid>
          </Grid>

          {dataEvents && dataEvents?.events.length > 0 && (
            <Box mb={6}>
              <Typography variant="h2" textAlign="center" mb={4}>
                Les activités de l'étape de {data && data?.stage?.name}
              </Typography>
              <Box textAlign={'center'} marginBottom={6}>
                <ToggleButtonGroup
                  color="primary"
                  value={selectedButton}
                  exclusive
                  onChange={(e, v) => v !== null && setSelectedButton(v)}
                >
                  <ToggleButton value="list">Liste</ToggleButton>
                  <ToggleButton value="map">Carte</ToggleButton>
                  <ToggleButton value="calendar">Calendrier</ToggleButton>
                </ToggleButtonGroup>
              </Box>

              {selectedButton == 'list' && data && data.stage && (
                <Stack spacing={2} py={4}>
                  {dataEvents &&
                    dataEvents?.events.map((event) => {
                      return <EventCard key={event.id} event={event} stageName={data?.stage?.name} />;
                    })}
                  {dataEvents && dataEvents?.events?.length < 1 && <Typography>Aucune activité</Typography>}
                </Stack>
              )}
              {selectedButton == 'map' && data && data.stage && (
                <Box sx={{ width: '100%', height: '400px' }}>
                  <MapWithNoSSR
                    ref={mapRef}
                    position={[data.stage.lat, data.stage.lng]}
                    zoom={14}
                    scrollWheelZoom={false}
                    id="map"
                    style={{ height: '100%' }}
                  >
                    <MarkerClusterWithNoSSR>
                      {dataEvents?.events?.map((event) => {
                        return <MarkerEventWithNoSSR event={event} stageName={data?.stage?.name} />;
                      })}
                    </MarkerClusterWithNoSSR>
                  </MapWithNoSSR>
                </Box>
              )}

              {selectedButton == 'calendar' && (
                <Calendar
                  events={events}
                  stage={data.stage}
                  startDate={moment(parseInt(data.stage.startedAt))}
                  withViewSwitcher={false}
                  withAddEvent={canEdit()}
                />
              )}
            </Box>
          )}

          <Grid container spacing={6} justifyContent="center" mb={4}>
            <Grid item xs={12} md={6} lg={4}>
              <ActionCard
                imageSrc="/image/home/Tour-alternatiba-aide-organisation-adrien-marie.jpg"
                alt="Tour Alternatiba 2018"
                title="Aide nous !"
                content="Rejoins l'organisation de l'étape"
                buttonText="Je participe"
                buttonLink={data?.stage.volunteerForm || ''}
                openNewTab
                onButtonClick={
                  data?.stage.volunteerAction !== 'externForm' ? () => setVolunteerModalOpen(true) : undefined
                }
              />
            </Grid>
          </Grid>

          <StageVolunteerModal
            stageId={data?.stage.id}
            open={volunteerModalOpen}
            onClose={() => setVolunteerModalOpen(false)}
          />

          {data?.stage?.pictures && data.stage.pictures.filter((p) => !p.main && !p.partner).length > 0 && (
            <>
              <Typography variant="h4" textAlign="center">
                Photos
              </Typography>
              <ImagesList
                pictures={data.stage.pictures
                  .filter((p) => !p.main && !p.partner)
                  .map((p) => ({ src: getImageUrl(p.originalPicturePath) }))}
              />
            </>
          )}

          {data?.stage?.pictures && data.stage.pictures.filter((p) => !p.main && p.partner).length > 0 && (
            <>
              <Typography variant="h4" textAlign="center">
                Partenaires
              </Typography>
              <ImagesList
                pictures={data.stage.pictures
                  .filter((p) => !p.main && p.partner)
                  .map((p) => ({ src: getImageUrl(p.originalPicturePath) }))}
              />
            </>
          )}
        </StageContent>

        {canEdit() && (
          <Link href={`/admin/stages/${data?.stage.id}`}>
            <Fab
              color="green"
              aria-label="edit"
              sx={{
                position: 'fixed',
                bottom: '40px',
                right: '40px',
                zIndex: '1400',
              }}
            >
              <EditIcon />
            </Fab>
          </Link>
        )}
      </Box>
    </AppLayout>
  );
};

export default withApollo()(Stage);
// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// revalidation is enabled and a new request comes in
export async function getServerSideProps(ctxt) {
  const res = await fetch(process.env.NEXT_PUBLIC_API_URI, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      operationName: 'stage',
      variables: {
        name: ctxt.params.name,
      },

      query: GET_STAGE_SSR,
    }),
  });

  const initialData = await res.json();

  if (initialData.errors) {
    console.error(` Error fetching stage id ${ctxt.params.id} error message : ${initialData.errors[0].message}`);
  }

  return {
    props: { initialData },
  };
}
