import React from 'react';
import { Button, Card, CardActions, CardContent, CardMedia, Link, Typography } from '@mui/material';
import Image from 'next/image';

type Props = {
  title: string;
  imageSrc?: string;
  alt?: string;
  content?: string;
  buttonText: string;
  buttonLink: string;
  openNewTab?: boolean;
  onButtonClick?: () => void;
};

const ActionCard = ({ imageSrc, alt, title, content, buttonText, buttonLink, openNewTab, onButtonClick }: Props) => (
  <Card>
    {imageSrc && imageSrc !== 'undefined' && (
      <CardMedia sx={{ aspectRatio: '1.6', position: 'relative' }}>
        <Image src={imageSrc} alt={alt || title} fill objectFit="cover" />
      </CardMedia>
    )}
    <CardContent>
      <Typography variant="h3" color="secondary.contrastText" sx={{ fontSize: '21px' }}>
        {title}
      </Typography>
      {content && <Typography color="secondary.contrastText">{content}</Typography>}
    </CardContent>
    <CardActions>
      {onButtonClick ? (
        <Button
          variant="hoverScaled"
          color="primary"
          sx={{ margin: 'auto' }}
          onClick={onButtonClick}
        >
          {buttonText}
        </Button>
      ) : (
        <Button
          variant="hoverScaled"
          color="primary"
          sx={{ margin: 'auto' }}
          component={Link}
          href={buttonLink}
          target={openNewTab ? '_blank' : undefined}
        >
          {buttonText}
        </Button>
      )}
    </CardActions>
  </Card>
);

export default ActionCard;
