export default {
  'velorution': '🚲 Vélorution',
  'village': '🏘 Village des alternatives',
  'transformation': '✊ Transformation',
  'spectacle': '🎤 Concert, Spectacle',
  'atelier': '🎓 Formation, Atelier, Table-ronde',
  'dcnv': '🖐 Formation à la désobéissance civile non-violente',
  'conference': '✨ Conférence du Tour Alternatiba',
  'other': '👀 Autre',
};
