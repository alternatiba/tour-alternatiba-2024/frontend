import React, { useCallback } from 'react';
import { makeStyles } from '@mui/styles';
import { Grid, Link, Typography } from '@mui/material';
import { useRouter } from 'next/router';
import ImageContainer from 'components/ImageContainer';
import Image from 'next/image';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: '2em',
  },
  partners: {
    padding: '2em 0',
    textAlign: 'center',
    cursor: 'pointer',
    [theme.breakpoints.down('lg')]: {
      padding: '1em 0',
    },
    width: '100%',
  },
  title: {
    textAlign: 'center',
    fontSize: '32px',
  },
}));

const partnersData = [
  { href: 'https://www.biocoop.fr/nos-engagements/fonds-de-dotation-biocoop', src: '/image/partenaires/biocoop-fonds-dotation.png', alt: 'Logo – Biocoop Fonds de Dotation' },
  
  { href: 'https://emmaus-france.org/', src: '/image/partenaires/emmaus-france.jpg', alt: 'Logo – Emmaüs France' },
  { href: 'https://www.fondation-abbe-pierre.fr/', src: '/image/partenaires/fondation-abbe-pierre.jpg', alt: 'Logo – Fondation Abbé Pierre' },
  { href: 'https://www.oxfamfrance.org/', src: '/image/partenaires/oxfam-france.png', alt: 'Logo – Oxfam France' },
  { href: 'https://solidaires.org/', src: '/image/partenaires/union-syndicale-solidaires.png', alt: 'Logo – Union Syndicale Solidaires' },
  { href: 'https://www.greenpeace.fr/', src: '/image/partenaires/greenpeace-france.jpeg', alt: 'Logo – Greenpeace France' },

  { href: 'https://fondation-mecenat-leanature.org/', src: '/image/partenaires/fondation-lea-nature.png', alt: 'Logo – Fondation Léa Nature' },
  { href: 'https://www.lilo.org/', src: '/image/partenaires/lilo.png', alt: 'Logo – Lilo' },
  { href: 'https://extinctionrebellion.fr/', src: '/image/partenaires/xr.png', alt: 'Logo – Extinction Rébellion France' },
  { href: 'https://extinctionrebellion.fr/', src: '/image/partenaires/rap.png', alt: 'Logo – Résistance à l\'Agression Publicitaire' },
  { href: 'https://www.helloasso.com/', src: '/image/partenaires/helloasso.png', alt: 'Logo – Hello Asso' },
  { href: 'https://bassinesnonmerci.fr/', src: '/image/partenaires/bnm.png', alt: 'Logo – Collectif Bassines Non Merci' },
  { href: 'https://ressourceries.info/?PagePrincipale', src: '/image/partenaires/rrrr.png', alt: 'Logo – Réseau National des Ressourceries et Recycleries' },
  { href: 'https://scientifiquesenrebellion.fr/', src: '/image/partenaires/scientifique_rebellion.png', alt: 'Logo – Scientifiques en rébellion' },
  { href: 'https://www.trimartolod.fr/', src: '/image/partenaires/trimartolod.png', alt: 'Logo – Brasserie Tri Martolod' },
  { href: 'https://www.mybeautydate.com/brands/16010010997470', src: '/image/partenaires/la-fleur-des-chateaux.png', alt: 'Logo – La Fleur des Châteaux' },
  { href: 'https://www.sudeducation12.org/', src: '/image/partenaires/sudaveryon.png', alt: 'Logo – SUD éducation 12 Aveyron' },

  { href: 'https://www.au5v.fr/', src: '/image/partenaires/au5v.png', alt: 'Logo – Association des Usagers du Vélo, des Voies Vertes et Véloroutes des Vallées de l’Oise' },
  { href: 'https://alternatibamarseille.org/marsmob-2', src: '/image/partenaires/marsmob.png', alt: 'Logo – MarsMob' },
  { href: 'https://www.eau-et-rivieres.org/', src: '/image/partenaires/eau_riviere.png', alt: 'Logo – Eau et Rivières de Bretagne' },
  { href: 'https://www.facebook.com/p/Douar-Bev-100086474730351/', src: '/image/partenaires/douar_bev.png', alt: 'Logo – Douar Bev' },
  { href: 'https://www.facebook.com/letournevis/', src: '/image/partenaires/letournevis.png', alt: 'Logo – Le Tournevis' },
  { href: 'https://amap-aura.org/', src: '/image/partenaires/amap_auvergne.png', alt: 'Logo – Réseau AMAP Auvergne-Rhône-Alpes' },
  { href: 'https://www.facebook.com/p/La-Galette-Monnaie-Locale-100092280930603', src: '/image/partenaires/lagalette.png', alt: 'Logo – La Galette, Monnaie Locale' },
  { href: 'https://www.plancton-du-monde.org/', src: '/image/partenaires/plancton.png', alt: 'Logo – Plancton du Monde' },
  { href: 'https://transition.lu/', src: '/image/partenaires/transition_lu.png', alt: 'Logo – transition.lu' },

  { href: 'https://www.alda.eus/', src: '/image/partenaires/alda.png', alt: 'Logo - Alda' },
  { href: 'https://amap-bfc.org/', src: '/image/partenaires/amap-bfc.jpg', alt: 'Logo - AMAP - Bourgogne Franche-Comté' },
  { href: 'https://www.heureux-cyclage.org/', src: '/image/partenaires/heureux-cyclage.png', alt: 'Logo - L\'Heureux Cyclage' },
  { href: 'https://artisansdumonde.org/', src: '/image/partenaires/artisans-du-monde.png', alt: 'Logo - Artisans du Monde' },
  { href: 'https://extinctionrebellion.fr/branches/nantes/', src: '/image/partenaires/xr-nantes.png', alt: 'Logo - Extinction Rébellion Nantes' },
  { href: 'https://www.amisdelaterre.org/', src: '/image/partenaires/amis-de-la-terre.png', alt: 'Logo - Les amis de la Terre France' },
  { href: 'https://nonviolence.fr/', src: '/image/partenaires/man.jpg', alt: 'Logo - Mouvement pour une Alternative Non-Violente' },
  { href: 'https://www.bio-equitable-en-france.fr/', src: '/image/partenaires/bio-equitable-en-france.jpeg', alt: 'Logo - Bio Équitable en France' },
  { href: 'https://www.agirpourlenvironnement.org/', src: '/image/partenaires/agir-pour-l-environnement.png', alt: 'Logo - Agir pour l\'Environnement' },
  { href: 'https://www.ldh-france.org/', src: '/image/partenaires/ldh.png', alt: 'Logo - LDH' },
  { href: 'https://terredeliens.org/languedoc-roussillon/', src: '/image/partenaires/terre-de-lien-lr.png', alt: 'Logo - Terre de liens Languedoc-Roussillon' },
  { href: 'https://nonviolence21.org/', src: '/image/partenaires/non-violence-xxi.png', alt: 'Logo - Non-Violence XXI' },
  { href: 'https://www.sol-reseau.org/', src: '/image/partenaires/sol.png', alt: 'Logo - Réseau SOL' },
  { href: 'https://www.geres.eu/', src: '/image/partenaires/geres.png', alt: 'Logo - Feres' },
  { href: 'https://www.gafe-haiti.org/', src: '/image/partenaires/gafe.png', alt: 'Logo - Groupe d\'action Francophone pour l\'Environnement' },
  { href: 'https://reclaimfinance.org/site/', src: '/image/partenaires/reclaim-france.png', alt: 'Logo - Reclaim Finance' },
  { href: 'https://mouvementutopia.org/', src: '/image/partenaires/utopia.jpg', alt: 'Logo - Mouvement Utopia' },
  { href: 'https://tierslien.com/', src: '/image/partenaires/tiers-lien.png', alt: 'Logo - Le Tiers Lien' },
  { href: 'https://www.facebook.com/alofatuvaluofficial/', src: '/image/partenaires/alofa-tuvalu.jpg', alt: 'Logo - Alofa Tuvalu' },
  { href: '/', src: '/image/partenaires/albergerie.jpg', alt: 'Logo - L\'albergerie' }
];

const Partner = () => {
  const styles = useStyles();
  const router = useRouter();

  const handleClickAbout = useCallback(() => {
    router.push('/about');
  }, [router]);

  return (
    <Grid container className={styles.container} justifyContent="center">
      <Grid item container justifyContent="center" className={styles.title}>
        <Typography variant="greenTitle" sx={{ fontSize: { xs: '30px', md: '40px' } }}>
          Nos partenaires
        </Typography>
      </Grid>
      <Grid item container direction="row" justifyContent="space-around" className={styles.partners} xs={10} sm={6}>
        {partnersData.map((partner, index) => (
          <Grid item key={index}>
            <Link href={partner.href} target="_blank">
              <ImageContainer
                noBorder
                sx={{
                  position: 'relative',
                  height: 0,
                  paddingTop: '100%', // This maintains a 1:1 aspect ratio
                  width: '100px',
                  mx: 'auto',
                }}
              >
                <Image src={partner.src} alt={partner.alt} layout="fill" objectFit="contain" />
              </ImageContainer>
            </Link>
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default Partner;
