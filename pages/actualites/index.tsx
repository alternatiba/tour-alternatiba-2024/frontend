/* eslint-disable no-console */
import React, { useEffect, useState } from 'react';
import { Button, Container, Grid, Typography, useTheme, useMediaQuery, Stack } from '@mui/material';
import AboutUsCard from '../../components/cards/AboutUsCard';
import ArticleCard from '../../components/cards/ArticleCard';
import AppLayout from '../../containers/layouts/AppLayout';
import Header from 'containers/layouts/homePage/header';

const GET_ARTICLES = `
  query articles {
    articles(sort: "createdAt", way: "DESC") {
      id
      label
      shortDescription
      createdAt
      pictures {
        id
        originalPicturePath
        main
      }
    }
  }
`;

type Props = {
  articles: {
    id: number;
    label: string;
    shortDescription: string;
    createdAt: number;
    pictures: {
      id: number;
      originalPicturePath: string;
      main: boolean;
    }[];
  }[];
};

const medias: Record<string, { name: string; logo: string; logoWidth: string; logoHeight: string }> = {
  republicainLorrain: { name: 'Le Républicain Lorrain', logo: '/image/medias/republicain-lorrain.png', logoWidth: '394', logoHeight: '128' },
  alterEco: { name: 'Alternatives Économiques', logo: '/image/medias/alter-eco.jpg', logoWidth: '800', logoHeight: '399' },
  voixDuNord: { name: 'La Voix du Nord', logo: '/image/medias/voix-du-nord.png', logoWidth: '500', logoHeight: '500' },
  reporterre: { name: 'Reporterre', logo: '/image/medias/reporterre.png', logoWidth: '280', logoHeight: '88' },
  france3: { name: 'France 3', logo: '/image/medias/france3.png', logoWidth: '2560', logoHeight: '483' },
  liberation: { name: 'Libération', logo: '/image/medias/liberation.svg', logoWidth: '516', logoHeight: '188' },
  vertLeMedia: { name: 'Vert le Média', logo: '/image/medias/vertLeMedia.png', logoWidth: '600', logoHeight: '202' },
  ouestFrance: { name: 'Ouest France', logo: '/image/medias/ouest-france.svg', logoWidth: '203', logoHeight: '66' },
  estRepublicain: { name: 'L\'Est Républicain', logo: '/image/medias/est-republicain.svg', logoWidth: '700', logoHeight: '344' },
  mediapart: { name: 'Mediapart', logo: '/image/medias/mediapart.png', logoWidth: '2560', logoHeight: '462' },
  sudOuest: { name: 'Sud Ouest', logo: '/image/medias/sud-ouest.svg', logoWidth: '1001', logoHeight: '746' },
  relevePeste: { name: 'La Relève et la Peste', logo: '/image/medias/releve-peste.svg', logoWidth: '237', logoHeight: '22' },
  journalGers: { name: 'Le journal du Gers', logo: '/image/medias/journal-gers.jpg', logoWidth: '360', logoHeight: '120' },
  midiLibre: { name: 'Midi Libre', logo: '/image/medias/midi-libre.png', logoWidth: '252', logoHeight: '60' },
  franceBleu: { name: 'France Bleu', logo: '/image/medias/france-bleu.svg', logoWidth: '500', logoHeight: '500' },
  marseillaise: { name: 'La Marseillaise', logo: '/image/medias/marseillaise.svg', logoWidth: '540', logoHeight: '100' },
};

const buildDate = (day: string, month: string, year: string) => {
  return new Date(month + '/' + day + '/' + year);
};

const aboutUsList = [
  {
    media: medias['marseillaise'],
    title: 'Le Tour Alternatiba, lanceur d\'alerte climatique et social',
    publishedAt: buildDate('19', '09', '2024'),
    link: 'https://www.lamarseillaise.fr/societe/le-tour-alternatiba-lanceur-d-alerte-climatique-et-social-NF16640629',
  },
  {
    media: medias['franceBleu'],
    title: 'VIDEO\u00A0– Juliette Caroulle, participante du Tour de France Alternatiba 2024',
    publishedAt: buildDate('03', '09', '2024'),
    link: 'https://www.radiofrance.fr/francebleu/podcasts/l-invite-du-jour/video-juliette-caroulle-participante-du-tour-de-france-alternatiba-2024-3716361',
  },
  {
    media: medias['midiLibre'],
    title: 'Caravane cycliste de Nantes à Marseille, le Tour Alternatiba a dénoncé le Contournement Ouest de Montpellier',
    publishedAt: buildDate('02', '09', '2024'),
    link: 'https://www.midilibre.fr/2024/09/02/caravane-cycliste-de-nantes-a-marseille-le-tour-alternatiba-a-denonce-le-contournement-ouest-de-montpellier-12172655.php',
  },
  {
    media: medias['journalGers'],
    title: 'Alternatiba se mobilise contre le bétonnage d\'une terre agricole à Masseube',
    publishedAt: buildDate('29', '08', '2024'),
    link: 'https://lejournaldugers.fr/article/78006-alternatiba-se-mobilise-contre-le-betonnage-dune-terre-agricole-a-masseube',
  },
  {
    media: medias['relevePeste'],
    title: 'Pays basque\u00A0: «\u00A0Stop aux résidences secondaires\u00A0»',
    publishedAt: buildDate('19', '08', '2024'),
    link: 'https://lareleveetlapeste.fr/pays-basque-stop-aux-residences-secondaires',
  },
  {
    media: medias['sudOuest'],
    title: '«\u00A0On peut se mobiliser collectivement et dans la joie\u00A0»\u00A0: à Bayonne, les sourires militants du Tour Alternatiba',
    publishedAt: buildDate('17', '08', '2024'),
    link: 'https://www.sudouest.fr/pyrenees-atlantiques/bayonne/on-peut-se-mobiliser-collectivement-et-dans-la-joie-a-bayonne-les-sourires-militants-du-tour-alternatiba-21055682.php',
  },
  {
    media: medias['mediapart'],
    title: 'Le vélo, accélérateur insoupçonné des luttes sociales',
    publishedAt: buildDate('04', '08', '2024'),
    link: 'https://www.mediapart.fr/journal/france/040824/le-velo-accelerateur-insoupconne-des-luttes-sociales',
  },
  {
    media: medias['estRepublicain'],
    title: 'Alternatiba débarque ce samedi place Granvelle pour sensibiliser sur le thème de l\'eau',
    publishedAt: buildDate('13', '07', '2024'),
    link: 'https://www.estrepublicain.fr/environnement/2024/07/13/alternatiba-debarque-ce-samedi-place-granvelle-pour-sensibiliser-sur-le-theme-de-l-eau',
  },
  {
    media: medias['france3'],
    title: '«\u00A0Besançon, soyons poissons\u00A0»\u00A0: tout savoir sur la journée de l\'eau organisée samedi par Alternatiba',
    publishedAt: buildDate('11', '07', '2024'),
    link: 'https://france3-regions.francetvinfo.fr/bourgogne-franche-comte/doubs/besancon/besancon-soyons-poissons-tout-savoir-sur-la-journee-de-l-eau-organisee-samedi-par-alternatiba-3002663.html',
  },
  {
    media: medias['republicainLorrain'],
    title: 'Le Tour Alternatiba fait étape aux Frigos de Metz\u00A0: «\u00A0Chacun de nous peut agir pour l’environnement\u00A0»',
    publishedAt: buildDate('08', '07', '2024'),
    link: 'https://www.republicain-lorrain.fr/culture-loisirs/2024/07/08/le-tour-alternatiba-fait-etape-aux-frigos-de-metz-chacun-de-nous-peut-agir-pour-l-environnement',
  },
  {
    media: medias['alterEco'],
    title: 'Le tour de France très politique d’Alternatiba',
    publishedAt: buildDate('02', '07', '2024'),
    link: 'https://www.alternatives-economiques.fr/tour-de-france-tres-politique-dalternatiba/00111645',
  },
  {
    media: medias['liberation'],
    title: '«\u00A0On est au bord du précipice\u00A0»\u00A0: sur le tour Alternatiba, les militants écolos entre peur et détermination face au risque du RN',
    publishedAt: buildDate('29', '06', '2024'),
    link: 'https://www.liberation.fr/environnement/on-est-au-bord-du-precipice-sur-le-tour-alternatiba-les-militants-ecolos-entre-peur-et-determination-face-au-risque-du-rn-20240626_QPJANEVZFZBGRHVOH2TDGTJZMI',
  },
  {
    media: medias['voixDuNord'],
    title: 'Dunkerque et Grande-Synthe accueillent le tour Alternatiba ce week-end',
    publishedAt: buildDate('21', '06', '2024'),
    link: 'https://www.lavoixdunord.fr/1475143/article/2024-06-20/dunkerque-et-grande-synthe-accueillent-le-tour-alternatiba-ce-week-end',
  },
  {
    media: medias['reporterre'],
    title: '«\u00A0Il faut apporter de l’espoir\u00A0»\u00A0: Alternatiba repart pour un Tour de France',
    publishedAt: buildDate('03', '06', '2024'),
    link: 'https://reporterre.net/Le-Tour-de-France-des-alternatives-se-remet-en-selle',
  },
  {
    media: medias['france3'],
    title: 'Changement climatique et justice sociale\u00A0: le départ du tour Alternatiba',
    publishedAt: buildDate('03', '06', '2024'),
    link: 'https://france3-regions.francetvinfo.fr/pays-de-la-loire/loire-atlantique/nantes/changement-climatique-et-justice-sociale-le-depart-du-tour-alternatiba-2979539.html',
  },
  {
    media: medias['liberation'],
    title:
      'Troisième tour de France d’Alternatiba\u00A0: «\u00A0Un mouvement motivant, qui apporte de l’énergie, un moment d’accès à l’engagement pour plein de gens\u00A0»',
    publishedAt: buildDate('02', '06', '2024'),
    link: 'https://www.liberation.fr/environnement/troisieme-tour-de-france-dalternatiba-un-mouvement-motivant-qui-apporte-de-lenergie-un-moment-dacces-a-lengagement-pour-plein-de-gens-20240601_L6BPNIZCB5DUTEFDUUZSV3WLIE',
  },
  {
    media: medias['vertLeMedia'],
    title:
      'Quatre mois, 5 500 kilomètres, une centaine d’étapes\u00A0: le Tour Alternatiba 2024 s’élancera de Nantes ce dimanche',
    publishedAt: buildDate('31', '05', '2024'),
    link: 'https://vert.eco/articles/quatre-mois-5-500-kilometres-une-centaine-detapes-le-tour-alternatiba-2024-selancera-de-nantes-ce-dimanche',
  },
  {
    media: medias['ouestFrance'],
    title: 'Climat. Alternatiba déploie une banderole à Nantes avec le tracé de son grand tour de France à vélo',
    publishedAt: buildDate('24', '01', '2024'),
    link: 'https://www.ouest-france.fr/environnement/climat-alternatiba-deploie-une-banderole-a-nantes-avec-le-trace-de-son-grand-tour-de-france-a-velo-2015f696-baa1-11ee-9ea4-b02fbeb9c343',
  },
];

const News = ({ articles }: Props) => {
  const [showedArticles, setShowedArticles] = useState(articles.slice(0, 3));
  const [expanded, setExpanded] = useState(false);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));

  useEffect(() => {
    if (expanded) {
      setShowedArticles(articles);
    }
  }, [expanded]);

  return (
    <AppLayout>
      <Header title="Les actualités du Tour Alternatiba" />

      <Container maxWidth="xl" sx={{ marginTop: { xs: 3, md: 6 }, marginBottom: 6 }}>
        <Grid container spacing={4} sx={{ mb: 5 }}>
          {showedArticles.map((article) => (
            <Grid item key={article.id} xs={12} sm={6} md={4}>
              <ArticleCard article={article} />
            </Grid>
          ))}
        </Grid>

        {articles.length > 3 && !expanded && (
          <Button variant="contained" color="primary" size="large" onClick={() => setExpanded(true)}>
            Voir tous les articles
          </Button>
        )}

        <Typography variant="greenTitle" textAlign="center" sx={{ fontSize: { xs: '30px', md: '40px' } }}>
          On parle de nous !
        </Typography>

        <Stack spacing={matches ? 2 : 1}>
          {aboutUsList.map((aboutUs, index) => (
            <div key={index}>
              <AboutUsCard {...aboutUs} />
            </div>
          ))}
        </Stack>
      </Container>
    </AppLayout>
  );
};

export default News;

export async function getServerSideProps() {
  const res = await fetch(process.env.NEXT_PUBLIC_API_URI || '', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query: GET_ARTICLES,
    }),
  });

  const initialData = await res.json();

  if (initialData.errors) {
    console.error(`Error fetching articles, error message : ${initialData.errors[0].message}`);
  }

  return {
    props: {
      articles: initialData?.data?.articles || [],
    },
  };
}
