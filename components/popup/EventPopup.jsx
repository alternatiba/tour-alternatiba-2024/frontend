import React, { useState } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { Grid, Typography } from '@mui/material';
import Image from 'next/image';
import Favorite from '../Favorite';
import { getImageUrl } from '../../utils/utils';
import Moment from 'react-moment';
import eventCategories from 'src/eventCategories';

const useHeaderStyles = makeStyles(() => ({
  header: (props) => ({
    position: 'relative',
    height: props.errorLoading ? 'auto' : 100,
    '& > *': {
      zIndex: 1
    }
  }),
  categorie: (props) => ({
    backgroundColor: 'white',
    padding: '0 5px 0 5px',
    textAlign: 'center',
    border: `1px solid ${props.color}`,
    '& p': {
      margin: 0
    }
  }),
}));

const Header = ({  }) => {
  const [errorLoading, setErrorLoading] = useState(!!!event?.pictures?.[0]?.originalPicturePath);
  const styles = useHeaderStyles({
    errorLoading,
    color: event?.entries?.[0]?.parentEntry?.color || 'rgba(128, 128, 128, 0.15)'
  });

  return (
    <Grid container direction="row" justifyContent="space-between" alignItems="flex-start" className={styles.header}>
      {
        event?.pictures?.[0]?.originalPicturePath && (
          <Image
            src={getImageUrl(event.pictures[0].originalPicturePath)}
            layout="fill"
            objectFit="contain"
            onError={() => {
              setErrorLoading(true);
            }}
          />
        )
      }
      {
        event?.entries?.[0]?.label && (
          <Grid item className={styles.categorie}>
            <Typography
              style={{ color: event?.entries?.[0]?.parentEntry?.color }}
              gutterBottom
            >
              {event?.entries?.[0]?.label}
            </Typography>
          </Grid>
        )
      }
    </Grid>
  )
};

const useStyles = makeStyles((theme) => ({

  titleDiv: {
    display: 'flex',
    alignItems: 'center',
  },
  title: {
    textAlign: 'left',
    color: 'black',
    width: '100%',
  },
  icon: {
    color: '#bd0b3d',
    width: '20px',
    marginRight: 6
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  buttonGrid: {
    [theme.breakpoints.up('sm')]: {
      margin: '0.5em 0 1.5em 0',
    },
    color: 'white',
    backgroundColor: '#0b8253',
    border: 'none',
    padding: '0 3em',
    height: '2.5em',
    width: 'fit-content',
    '&:hover': {
      cursor: 'pointer',
      color: '#0b8253',
      backgroundColor: 'white',
      border: '2px solid #0b8253',
      fontSize: '1em',
      textDecoration: 'none!important',
    },
    backgroundSize: '14%',
  },
  shortDescription: {
    maxWidth:"150px",
    fontSize: '1em',
  },
}));

const EventPopup = ({ event, stageName, onMouseOut, tooltip = false }) => {
  const styles = useStyles();

  return (
    <Grid container direction="column" onMouseLeave={onMouseOut}>

      <div className={styles.content}>
        <Grid container>
          <Grid item>
            <div className={styles.titleDiv}>
              <Typography
                variant="h6"
                component="h2"
                className={styles.title}
              >
               {event?.label}
              </Typography>
            </div>
          </Grid>

        </Grid>
        {eventCategories[event.category]}
        <br/>
          <Moment
            locale="fr"
            format="dddd DD MMMM YYYY"
            unix
          >
            {event.startedAt / 1000}
          </Moment>
          {new Date(event.endedAt).toDateString() !== new Date(event.startedAt).toDateString() && (
            <>
              <span> au </span>
              <Moment
                locale="fr"
                format="DD MMMM YYYY"
                unix
              >
                {event.endedAt / 1000}
              </Moment>
            </>
          )}
        <Typography component="p" className={styles.shortDescription}>
          {event && event.shortDescription}
        </Typography>
      </div>

      {
        !tooltip && (
          <a href={`/etape/${stageName}/activite/${event.id}`} target="_blank" rel="noreferrer" className={styles.buttonContainer}>
            <button className={styles.buttonGrid}>
              EN SAVOIR PLUS
            </button>
          </a>
        )
      }
    </Grid>
  );
};

export default EventPopup;
