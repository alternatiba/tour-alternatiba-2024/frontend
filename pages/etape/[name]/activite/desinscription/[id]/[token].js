import React from 'react';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import {
    Container,Link,
  } from '@mui/material';
import AppLayout from 'containers/layouts/AppLayout';
import Head from 'next/head';
import makeStyles from '@mui/styles/makeStyles';
import { withApollo } from 'hoc/withApollo.jsx';
import { Button } from '@mui/material';
import ClassicButton from 'components/buttons/ClassicButton';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';

const GET_EVENT = `
    query event($id: String) {
      event(id: $id) {
        id
        label
        description
        lat
        lng
        address
        city
        startedAt
        endedAt
        dateRule
        registerLink
        stages {
          id
          name
          address
          lat
          lng
          email
          startedAt
          pictures {
            id
            label
            originalPicturePath
            originalPictureFilename
            position
            logo
            main
          }
          referents {
            id
            surname
            lastname
          }
        }
      }
    }
  `;

  const REMOVE_PARTICIPANT_EVENT = gql`
  mutation removeParticipateEvent($token: String!, $eventId: Int!) {
    removeParticipateEvent(token: $token,eventId: $eventId)
  }
`;
  
const useStyles = makeStyles((theme) => ({
    formContainer: {
      marginTop: theme.spacing(8),
      marginBottom: theme.spacing(8),
    },
  }));
  
const UnregisterConfirmationPage = ({ initialData }) => {
    const router = useRouter();
    const { id,token } = router.query;
    const styles = useStyles();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const [
        removeParticipateEvent,
        { data: removeParticipateEventData, error: removeParticipateEventError, loading: removeParticipateEventLoading },
      ] = useMutation(REMOVE_PARTICIPANT_EVENT);
  
      useEffect(() => {
        if (!removeParticipateEventLoading && removeParticipateEventData) {
         
        } else if (removeParticipateEventError) {
          enqueueSnackbar('Une erreur s\'est produite, merci de bien vouloir réessayer.', {
            preventDuplicate: true,
          });
        }
      }, [removeParticipateEventLoading, removeParticipateEventError, removeParticipateEventData]);

      
    return (
        <AppLayout>
            <Head>
                <title>Désinscription de l'activité {initialData?.data?.event?.label} </title>
            </Head>
            <Container maxWidth="md" className={styles.formContainer}>
            <div>
                {!removeParticipateEventLoading && removeParticipateEventData && (
                    <div>
                        <h1>Vous êtes désinscrit·e de {initialData?.data?.label}  de l'étape {initialData?.data?.event?.stages[0].name}</h1>
                        <Button onClick={() => router.push(`/etape/${initialData?.data?.event?.stages[0].name}/activite/${initialData?.data?.event?.id}`)}>Retour à l'activité</Button> <Button onClick={() => router.push(`/etape/${initialData?.data?.event?.stages[0].name}`)}>Retour à l'étape</Button>
                    </div>
                )}
                {!removeParticipateEventData && (
                     <div>
                <h1>Désinscription de l'activité {initialData?.data?.event?.label}</h1>
                <p>Es-tu sûr·e de vouloir te désincrire de cette activité : 
                <br/>
                <Link target="_blank" href={`/etape/${initialData?.data?.event?.stages[0].name}/activite/${initialData?.data?.event?.id}`}> {initialData?.data?.event?.label} de l'étape de {initialData?.data?.event?.stages[0].name}</Link> le  {new Date(parseInt(initialData?.data?.event?.startedAt,10)).toLocaleDateString('fr-FR', { weekday: 'long', day: '2-digit', month: '2-digit' })} de  {new Date(parseInt(initialData?.data?.event?.startedAt,10)).toLocaleTimeString('fr-FR', {hour: '2-digit', minute: '2-digit' })}  à  {new Date(parseInt(initialData?.data?.event?.endedAt,10)).toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' })} ?
    

                </p>
                <ClassicButton onClick={() => {
                  removeParticipateEvent({
                    variables: {
                      token: token,
                      eventId: parseInt(initialData?.data?.event?.id),
                    },
                  });
                }}>Confirmer la désinscription</ClassicButton>
                </div>
            )}
            </div>
            </Container>
        </AppLayout>
    );
};

export default withApollo()(UnregisterConfirmationPage);


// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// revalidation is enabled and a new request comes in
export async function getServerSideProps(ctxt) {
   
    const res = await fetch(process.env.NEXT_PUBLIC_API_URI, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        operationName: 'event',
        variables: {
          id: ctxt.params.id,
        },
        query: GET_EVENT,
      }),
    });
  
    const initialData = await res.json();
    if (initialData.errors) {
      console.error(
        ` Error fetching event id ${ctxt.params.id
        } error message : ${initialData.errors[0].message
        }`,
      );
    }
   
    return {
      props: {
        initialData,
      },
    };
  }
  