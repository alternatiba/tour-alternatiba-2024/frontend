import React from 'react';
import ActionCard from './ActionCard';

type Props = {
  article: {
    id: number;
    label: string;
    shortDescription: string;
    createdAt: number;
    pictures: {
      id: number;
      originalPicturePath: string;
      main: boolean;
    }[];
  };
};

export default function ArticleCard({ article }: Props) {
  const { id, label, shortDescription, pictures } = article;

  const mainPicture = pictures.find((picture) => picture.main);

  return (
    <ActionCard
      imageSrc={`${process.env.NEXT_PUBLIC_URI}${mainPicture?.originalPicturePath}`}
      alt={label}
      title={label}
      content={shortDescription}
      buttonLink={`/article/${id}`}
      buttonText="Lire l'article"
    />
  );
}
