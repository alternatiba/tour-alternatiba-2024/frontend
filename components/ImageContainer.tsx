import React from 'react';
import { styled } from '@mui/material/styles';
import { Typography } from '@mui/material';

export const Caption: React.FC<{}> = ({ children }) => (
  <Typography
    variant="caption"
    component={'figcaption'}
    sx={{
      position: 'absolute',
      bottom: 0,
      textAlign: 'center',
      background: 'linear-gradient(0deg,rgba(0,0,0,.7),rgba(0,0,0,.3) 70%,transparent)',
    }}
    width="100%"
  >
    {children}
  </Typography>
);

type ImageContainerProps = {
  noBorder?: boolean;
};

const ImageContainer = styled('figure')<ImageContainerProps>(({ theme, noBorder }) => ({
  position: 'relative',
  border: noBorder ? 'none' : '5px solid',
  borderColor: theme.palette.primary.main,
}));

export default ImageContainer;
