import React, { useRef } from 'react';
import { Box, Button, Container, Grid, Link, Typography } from '@mui/material';
import Image from 'next/image';
import ImageContainer from 'components/ImageContainer';

const Newsletter = () => {
  const scrollToRef = useRef<HTMLDivElement>();

  return (
    <Container maxWidth="xl" sx={{ marginTop: { xs: 3, md: 6 }, marginBottom: 6 }}>
      <Grid container spacing={6}>
        <Grid item xs={12} lg={7}>
          <Grid container spacing={6} alignItems="center">
            <Grid item xs={12}>
              <Typography variant="greenTitle">Reçois les actualités du Tour Alternatiba</Typography>
              <Typography variant="paragraph">
                <strong>Le Tour Alternatiba 2024</strong> est une vaste mobilisation citoyenne de 4 mois pour
                s’organiser, se former, et adapter les territoires face aux conséquences du dérèglement climatique. Il
                est organisé par 2 associations qui font partie d’une même dynamique.
              </Typography>
              <Button
                variant="contained"
                sx={{ display: { xs: 'block', md: 'none' }, margin: 'auto', mt: 4 }}
                onClick={() => scrollToRef.current?.scrollIntoView({ behavior: 'smooth' })}
              >
                Voir le formulaire
              </Button>
            </Grid>
            <Grid item xs={12} md={6}>
              <ImageContainer
                noBorder
                sx={{ width: { xs: '60%', md: '80%' }, aspectRatio: '1061/618', margin: 'auto' }}
              >
                <Image src="/image/logo_alternatiba.png" alt="Logo Alternatiba" fill />
              </ImageContainer>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography variant="paragraph">
                <strong>Alternatiba</strong> est un mouvement citoyen pour le climat et la justice sociale. Avec un
                réseau de plus de 115 groupes locaux et de 12 alternatibases, lieux de transition sociale et écologique,
                nous sommes un acteur incontournable de la mobilisation climat depuis 2013.
              </Typography>
              <Typography variant="paragraph">
                Partout sur le territoire, notre mouvement met en lumière les alternatives concrètes pour lutter contre
                le dérèglement climatique et les relie. Partout, nous agissons pour un monde juste et soutenable qui ne
                laisse personne de côté.
              </Typography>
            </Grid>

            <Grid item xs={12} md={6}>
              <ImageContainer
                noBorder
                sx={{ width: { xs: '60%', md: '80%' }, aspectRatio: '1061/618', margin: 'auto' }}
              >
                <Image src="/image/logo_anv.png" alt="Logo ANV" fill />
              </ImageContainer>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography variant="paragraph">
                <strong>Action non-violente COP21</strong>, c’est un mouvement de citoyen·nes qui ne se résignent pas
                face au dérèglement climatique et aux injustices sociales.
              </Typography>
              <Typography variant="paragraph">
                Parce que c’est l’enjeu majeur de notre génération, nous organisons la résistance par l’action
                non-violente et, s’il le faut, par la désobéissance civile.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} lg={5} textAlign="center">
          <Box sx={{ width: { xs: '100%', md: 540 }, maxWidth: 540, margin: 'auto' }} ref={scrollToRef}>
            <iframe
              width="100%"
              height="830"
              src="https://fb6b0688.sibforms.com/serve/MUIFAMPBO0lX3tMDpetEHVWH18seGJbUPKEHDQ0JcYIGR7gghcZrz9j5NL-RPNIEkAmfngQ6JFU0oMlVL5AzUBwmm6HVpocYQ3ynF-fSlnEBSF9Pm3wISWky9tbqH9jQdyq5RaE0F2JU4XPpOz-BTbM7RZU_zcILbBfEmg-xzTWVAzJ3cP9f7Itrbw5uLKza5wgn7tOUo7RPx7FI"
              frameborder="0"
            ></iframe>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Newsletter;
