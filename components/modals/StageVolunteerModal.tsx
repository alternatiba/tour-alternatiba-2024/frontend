import React, { useCallback, useEffect } from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Stack } from '@mui/material';
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import TextInputField from 'components/fields/TextInputField';
import { useMutation } from '@apollo/client';
import gql from 'graphql-tag';
import { useSnackbar } from 'notistack';

const ADD_VOLUNTEER = gql`
  mutation AddStageVolunteer($stageId: Int!, $volunteerInfos: VolunteerInfos) {
    addStageVolunteer(stageId: $stageId, volunteerInfos: $volunteerInfos)
  }
`;

type VolunteerFields = {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  userNote: string;
};

type Props = {
  stageId: string;
  open: boolean;
  onClose: () => void;
};

const StageVolunteerModal: React.FC<Props> = ({ stageId, open, onClose }) => {
  const form = useForm<VolunteerFields>({
    mode: 'onTouched',
  });

  const {
    handleSubmit,
    formState: { errors },
  } = form;

  const [addVolunteer, { data, error, loading }] = useMutation(ADD_VOLUNTEER);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (!loading && data) {
      enqueueSnackbar("Votre demande de participation a bien été envoyée.", {
        preventDuplicate: true,
      });
    } else if (error) {
      enqueueSnackbar("Une erreur s'est produite, merci de bien vouloir réessayer.", {
        preventDuplicate: true,
      });
    }
  }, [data, error, loading]);

  const onSubmit: SubmitHandler<VolunteerFields> = useCallback((formValues) => {
    addVolunteer({
      variables: {
        stageId: parseInt(stageId, 10),
        volunteerInfos: {
          firstName: formValues.firstName,
          name: formValues.lastName,
          email: formValues.email,
          phone: formValues.phone || '',
          message: formValues.userNote || '',
        },
      },
    });
  }, []);

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      PaperProps={{
        component: 'form',
        onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
          event.preventDefault();
          handleSubmit(onSubmit)(event);
          onClose();
        },
      }}
      fullWidth
      maxWidth="sm"
    >
      <DialogTitle id="alert-dialog-title">Rejoindre l'organisation de cette étape</DialogTitle>
      <DialogContent>
        <FormProvider {...form}>
          <Stack direction="column" gap={2} mt={2}>
            <TextInputField
              name="firstName"
              label="Prénom"
              rules={{
                required: 'Prénom requis',
              }}
            />

            <TextInputField
              name="lastName"
              label="Nom"
              rules={{
                required: 'Nom requis',
              }}
            />

            <TextInputField
              name="email"
              label="Email"
              type="email"
              rules={{
                required: 'Email requis',
              }}
            />

            <TextInputField name="phone" label="Numéro de téléphone (facultatif)" />

            <TextInputField name="userNote" label="Message (facultatif)" multiline minRows={3} maxRows={6} />
          </Stack>
        </FormProvider>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary" variant="outlined">
          Annuler
        </Button>
        <Button type="submit" variant="contained" color="primary" autoFocus>
          Valider
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default StageVolunteerModal;
