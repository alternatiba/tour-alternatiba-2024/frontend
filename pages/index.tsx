import React from 'react';
import AppLayout from 'containers/layouts/AppLayout';
import { withApollo } from 'hoc/withApollo';
import Partner from '../containers/layouts/Partner';
import PresentationSection from '../containers/layouts/homePage/presentationSection';
import Header from '../containers/layouts/homePage/header';
import Newsletter from 'containers/layouts/Newsletter';

const Index = () => {
  return (
    <AppLayout>
      <Header title="Le Tour Alternatiba 2024 bat son plein !" />
      <PresentationSection id={'PresentationSection'} />

      <Newsletter />

      <Partner />
    </AppLayout>
  );
};

export default withApollo()(Index);
