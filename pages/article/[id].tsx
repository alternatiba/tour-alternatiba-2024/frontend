/* eslint-disable react/require-default-props */
/* eslint-disable no-console */
import React from 'react';
import { Box, Button, Container, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';
import AppLayout from '../../containers/layouts/AppLayout';
import Header from 'containers/layouts/homePage/header';
import ImagesList from 'components/ImagesList';
import { getImageUrl } from '../../utils/utils';
import RichTextContent from 'components/RichTextContent';

const GET_ARTICLE = `
  query article($id: String) {
    article(id: $id) {
      id
      label
      content
      shortDescription
      createdAt
      pictures {
        id
        label
        originalPicturePath
        main
      }
    }
  }
`;

const DateText = styled(Typography)(({ theme }) => ({
  textAlign: 'center',
  display: 'block',
  marginBottom: theme.spacing(1),
}));

type Props = {
  article: {
    id: number;
    label: string;
    content: string;
    shortDescription: string;
    createdAt: number;
    pictures: {
      id: number;
      originalPicturePath: string;
      main: boolean;
    }[];
  } | null;
};

const Article = ({ article }: Props) => {
  const mainPicture = article?.pictures.find((p) => p.main);

  const formatDate = (timestamp: string) => {
    const date = new Date(parseInt(timestamp, 10));
    const dateString = new Intl.DateTimeFormat('fr-FR', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    }).format(date);

    const timeString = new Intl.DateTimeFormat('fr-FR', {
      hour: 'numeric',
      minute: 'numeric',
    }).format(date);

    return `${dateString} à ${timeString}`;
  };

  if (!article || !mainPicture) {
    return (
      <AppLayout>
        <Container maxWidth="lg" sx={{ mb: 3, textAlign: 'center' }}>
          <Typography variant="greenTitle" textAlign="center" sx={{ fontSize: { xs: '30px', md: '40px' } }}>
            L'article n'existe pas
          </Typography>
          <Button component={Link} href="/actualites">
            Retour à la liste des actualités
          </Button>
        </Container>
      </AppLayout>
    );
  }

  const otherPictures = article.pictures.filter((p) => !p.main);

  return (
    <AppLayout>
      <Head>
      <title>{article.label}</title>
        <meta property="og:title" content={article.label} />
        <meta name="twitter:title" content={article.label} />
        <meta property="og:image" content={`${process.env.NEXT_PUBLIC_URI}${mainPicture.originalPicturePath}`} />
        <meta name="twitter:image" content={`${process.env.NEXT_PUBLIC_URI}${mainPicture.originalPicturePath}`} />

        <meta property="og:description" content={article.shortDescription} />
        <meta name="twitter:description" content={article.shortDescription} />
      </Head>

      <Header title={article.label} picture={`${process.env.NEXT_PUBLIC_URI}${mainPicture.originalPicturePath}`} />

      <Container maxWidth="lg" sx={{ mb: 3 }}>
        <DateText variant="overline">Publié le {formatDate(article.createdAt)}</DateText>

        <RichTextContent content={article.content} />

        {otherPictures.length > 0 && (
          <ImagesList pictures={otherPictures.map(p => ({ src: getImageUrl(p.originalPicturePath), label: article.label }))} />
        )}
      </Container>
    </AppLayout>
  );
};

export default Article;

export async function getServerSideProps(ctxt) {
  const res = await fetch(process.env.NEXT_PUBLIC_API_URI || '', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query: GET_ARTICLE,
      variables: {
        id: ctxt.params.id,
      },
    }),
  });

  const initialData = await res.json();
  if (initialData.errors) {
    console.error(`Error fetching article, error message : ${initialData.errors[0].message}`);
  }

  return {
    props: {
      article: initialData?.data?.article || null,
    },
  };
}
