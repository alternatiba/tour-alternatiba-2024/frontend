import React from 'react';
import { Box, Button, Container, Grid, Link, Typography } from '@mui/material';
import Image from 'next/image';
import AppLayout from 'containers/layouts/AppLayout';
import Header from 'containers/layouts/homePage/header';
import ImageContainer from 'components/ImageContainer';
import ActionCards from 'components/cards/ActionCards';

const About = () => {
  const renderImage = () => (
    <ImageContainer sx={{ width: { xs: '90%', md: '70%' }, aspectRatio: '1', margin: 'auto' }}>
      <Image src="/image/tour_photo2_sq.jpg" alt="Étape de Couffé – Tour Alternatiba 2018" fill />
    </ImageContainer>
  );

  return (
    <AppLayout>
      <Header title="Découvre l’équipe derrière le Tour Alternatiba" />

      <Container maxWidth="xl" sx={{ marginTop: { xs: 3, md: 6 }, marginBottom: 6 }}>
        <Grid container spacing={6} alignItems="center" rowSpacing={{ xs: 8, md: 16 }}>
          <Grid item xs={12} md={6}>
            <Typography variant="greenTitle">Le Tour Alternatiba 2024 pour transformer nos territoires !</Typography>
            <Typography variant="paragraph">
              <strong>Le Tour Alternatiba 2024</strong>, c’est une immense mobilisation citoyenne pour transformer
              ensemble les territoires de manière concrète, soutenable et désirable, face aux conséquences du
              dérèglement climatique que nous subissons déjà. À bord de nos vélos 3 et 4 places, nous avons pris
              le départ à Nantes le 2 juin, et parcourons les territoires, nous arrêtant dans les villes, les quartiers,
              les villages, les lieux-dits qui nous ouvrent leurs portes, jusqu'au 4 octobre à Marseille !
            </Typography>
            <Typography variant="paragraph">
              À chaque étape, nous nous mobilisons avec les habitantes, les habitants et collectifs de ces territoires
              afin de mettre en lumière les alternatives et les luttes locales qui existent, impulser une transformation
              concrète du territoire et former de nouvelles personnes à relever le défi climatique en passant à l’action.
            </Typography>
            <Typography variant="paragraph">
              Face à l'extrême droite et au macronisme qui lui pave la voie, l'espoir de la mobilisation citoyenne !
              Rejoins-nous !
            </Typography>
            <Button variant="hoverScaled" color="green" component={Link} href="itineraire" sx={{ marginTop: 4 }}>
              Découvre la carte
            </Button>
          </Grid>
          <Grid item xs={12} md={6} textAlign="center">
            <Box sx={{ margin: 'auto', width: { xs: '90%', md: '60%' } }}>
              <iframe
                width="100%"
                style={{ aspectRatio: '1' }}
                src="https://www.youtube-nocookie.com/embed/uAlSmJAzDdE?si=bQP1G3QG_nmZAOxa"
                title="YouTube video player"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                referrerPolicy="strict-origin-when-cross-origin"
                allowFullScreen
              ></iframe>
            </Box>
          </Grid>

          <Grid item xs={12} md={6} sx={{ display: { xs: 'none', md: 'block' } }}>
            {renderImage()}
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography variant="greenTitle">Alternatiba, c'est qui, c'est quoi ?</Typography>
            <Typography variant="paragraph">
              Alternatiba est un mouvement citoyen pour le climat et la justice sociale. Avec notre réseau de près de
              110 groupes locaux et de 12 bases de mobilisation, nous sommes un acteur incontournable de la mobilisation
              climat depuis 2013.
            </Typography>
            <Typography variant="paragraph">
              Partout sur le territoire, Alternatiba met en lumière les alternatives concrètes pour lutter contre le
              dérèglement climatique et les relie. Partout, le mouvement agit pour un monde juste et soutenable qui ne
              laisse personne de côté.
            </Typography>
            <Button
              variant="hoverScaled"
              color="green"
              component={Link}
              href="https://alternatiba.eu"
              sx={{ marginTop: 4 }}
            >
              Découvrir Alternatiba
            </Button>
          </Grid>
          <Grid item xs={12} md={6} sx={{ display: { xs: 'block', md: 'none' } }}>
            {renderImage()}
          </Grid>

          <Grid item xs={12} md={6}>
            <Typography variant="greenTitle">Nos soutiens</Typography>
            <Typography variant="paragraph">
              Le Tour Alternatiba est en grande partie financé grâce à la générosité des citoyen·nes et à notre campagne
              de financement participatif, dont l’objectif de 70 000 € a été largement dépassé !
            </Typography>
            <Typography variant="paragraph">
              Mais nous sommes aussi accompagné·es par des collectifs, des fondations, des entreprises engagées pour un
              futur plus juste et soutenable.
            </Typography>
          </Grid>
          <Grid container item xs={12} md={6} textAlign="center" justifyContent={'center'}>
            <Grid item xs={12} md={6} >
              <Link href="https://www.biocoop.fr/nos-engagements/fonds-de-dotation-biocoop">
                <ImageContainer
                  noBorder
                  sx={{
                    height: '75px',
                    width: '198px',
                    mx: 'auto',
                  }}
                >
                  <Image
                    src="/image/partenaires/biocoop-fonds-dotation.png"
                    alt="Logo – Biocoop Fonds de Dotation"
                    fill
                  />
                </ImageContainer>
              </Link>
            </Grid>
            <Grid item xs={12} md={5}>
              <Link href="https://www.trimartolod.fr/">
                <ImageContainer
                  noBorder
                  sx={{
                    height: '140px',
                    width: '182px',
                    mx: 'auto',
                    marginTop: { xs: 0, md: -1 },
                  }}
                >
                  <Image src="/image/partenaires/trimartolod.png" alt="Logo – Brasserie Tri Martolod" fill />
                </ImageContainer>
              </Link>
            </Grid>
            <Grid item xs={12} md={4}>
              <Link href="https://www.mybeautydate.com/brands/16010010997470">
                <ImageContainer
                  noBorder
                  sx={{
                    height: '125px',
                    width: '125px',
                    mx: 'auto',
                    marginTop: { xs: 0, md: -2 },
                  }}
                >
                  <Image src="/image/partenaires/la-fleur-des-chateaux.png" alt="Logo – La Fleur des Châteaux" fill />
                </ImageContainer>
              </Link>
            </Grid>
            <Grid item xs={12} md={5}>
              <Link href="https://fondation-mecenat-leanature.org/">
                <ImageContainer
                  noBorder
                  sx={{
                    height: '100px',
                    width: '252px',
                    mx: 'auto',
                  }}
                >
                  <Image src="/image/partenaires/fondation-lea-nature.png" alt="Logo – Fondation Léa Nature" fill />
                </ImageContainer>
              </Link>
            </Grid>
          </Grid>

          <ActionCards />
        </Grid>
      </Container>
    </AppLayout>
  );
};

export default About;
