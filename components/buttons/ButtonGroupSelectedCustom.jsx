import React, { useCallback, useState } from 'react';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import makeStyles from '@mui/styles/makeStyles';
import classNames from 'classnames';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'absolute',
    backgroundColor: '#f6f6f6',
    right: 10,
    bottom: 20,
    zIndex: 1001,
    [theme.breakpoints.down('md')]: {
      position: 'initial',
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 0,
      '& > *': {
        borderRadius: 0,
        boxShadow: 'none !important',
        flexBasis: ({ buttons }) => `calc(100% / ${buttons.length})`,
      },
      '& > *:first-child': {
        borderLeftWidth: 0,
      },
      '& > *:last-child': {
        borderRightWidth: 0,
      },
    },
  },
  '@media print': {
    root: {
      [theme.breakpoints.down('md')]: {
        display: 'none',
      },
    },
  },
}));

const ButtonGroupSelectedCustom = props => {
  const { buttons, className,selectedButton , onButtonChange} = props;
  const classes = useStyles(props);
 

  return (
    <ButtonGroup className={classNames(classes.root, className)}>
      {
        buttons.map(button => {
          return (
            <Button
              key={button.name}
              variant={selectedButton == button.name ? 'contained' : ''}
              classes={selectedButton === button.name ? classes.unselected : ''}
              onClick={() => {onButtonChange(button.name);}}
            >
              {button.label}
            </Button>
          )
        })
      }
    </ButtonGroup>
  )
};

export default ButtonGroupSelectedCustom;