
const withPWA = require("@ducanh2912/next-pwa").default({
  dest: "public",
});
module.exports = withPWA({
  reactStrictMode: false,
  eslint: {
    // Warning: Dangerously allow production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
  images: {
    domains: ['localhost', 'static.beta.tour.alternatiba.eu',
    'static.tour.alternatiba.eu',
    'static.prod.tour.alternatiba.eu'],
  },
  async rewrites() {
    return [
      {
        source: '/api/files',
        destination: process.env.NEXT_PUBLIC_API_URI.replace('/api/graphql', '/api/files')
      }
    ]
  },
  async redirects() {
    return [
      {
        source: '/stageAdmin',
        destination: '/admin/stages',
        permanent: true,
      },
      {
        source: '/stageAdmin/stage/:id',
        destination: '/admin/stages/:id',
        permanent: true,
      },
      {
        source: '/appel-bastamedia',
        destination: 'https://basta.media/climat-la-catastrophe-a-commence-organisons-la-justice-et-la-solidarite-appel-tour-alternatiba-2024-Alain-Damasio',
        permanent: true,
      },
      {
        source: '/don',
        destination: 'https://don.alternatiba.eu',
        permanent: true,
      },
      {
        source: '/kit-etape',
        destination: 'https://docs.google.com/document/d/13UpU99dWWe7uPKFeL8Z21owEGUQkfW7s9t5lCyNguE8',
        permanent: true,
      },
      {
        source: '/participer',
        destination: 'https://bdd.alternatiba.eu/form/qlbbiCDiexrtB2KblgClzfO0vCHvbW-lYogePZB5s3Y',
        permanent: true,
      },
      {
        source: '/kit-com-etape',
        destination: 'https://www.canva.com/design/DAF_OapmT84/yO-2YlSDLHDVrlK3m3ckig/view',
        permanent: true,
      },
      {
        source: '/pedaler',
        destination: 'https://docs.google.com/forms/d/e/1FAIpQLSf1mKB0m5ef-BJlhsaD-CS2nzGuk83a2xPgoKCACYX7kW_kRA/viewform',
        permanent: true,
      },
      {
        source: '/tutovelogenerateur',
        destination: 'https://www.youtube.com/watch?v=E6Bu_F-A2Ag',
        permanent: true,
      },
    ]
  },
});
