import React, { useState, useEffect } from 'react';
import { Box, Typography, Button, Link, useScrollTrigger, useTheme, useMediaQuery } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

const Banner = () => {
  const [visible, setVisible] = useState(false);
  const trigger = useScrollTrigger();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('md'));

  const handleClose = () => {
    setVisible(false);
    localStorage.setItem('bannerClosed', 'true');
  };

  useEffect(() => {
    if (localStorage.getItem('bannerClosed') === 'true') {
      setVisible(false);
    } else {
      setVisible(true);
    }
  }, []);

  if (!visible) return null;

  return (
    <Box
      sx={{
        width: '100%',
        backgroundColor: '#F3D53A',
        color: '#2D529E',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '16px',
        position: 'fixed',
        bottom: 0,
        zIndex: 1200,
        textAlign: 'center',
        flexDirection: isMobile ? 'column' : 'row',
      }}
    >
      <Typography
        variant="h6"
        sx={{
          fontFamily: 'Paytone One',
          marginBottom: isMobile ? '10px' : '',
          marginRight: isMobile ? '40px' : '20px',
          fontSize: 'large',
          textWrap: 'balance',
        }}
      >
        {/* Face à la montée des idées d’extrême droite, je soutiens le Tour Alternatiba */}
        Rendez-vous à Marseille les 4, 5 et 6 octobre pour l'arrivée du Tour Alternatiba
      </Typography>
      <Link
        // href="https://don.alternatiba.eu/tour-alternatiba-2024"
        href="https://alternatibamarseille.org/tour"
        target="_blank"
        rel="noopener"
        sx={{ textDecoration: 'none', marginRight: isMobile ? '40px' : '0px' }}
      >
        <Button
          variant="contained"
          sx={{
            backgroundColor: '#2D529E',
            color: '#F3D53A',
            fontFamily: 'Paytone One',
            '&:hover': {
              backgroundColor: '#E56794',
            },
          }}
        >
          {/* J’agis */}
          Site de la Cité des Possibles
        </Button>
      </Link>
      <Button
        onClick={handleClose}
        sx={{
          color: '#2D529E',
          position: 'absolute',
          right: '16px',
          top: isMobile ? '16px' : '8px',
          minWidth: 'auto',
          padding: '8px',
        }}
      >
        <CloseIcon fontSize={isMobile ? 'medium' : 'large'} />
      </Button>
    </Box>
  );
};

export default Banner;
