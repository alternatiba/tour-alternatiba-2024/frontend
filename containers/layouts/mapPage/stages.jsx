import React, { useCallback, useMemo, useRef ,useEffect } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import {
  Stack, IconButton, Tooltip, useMediaQuery, useTheme,
} from '@mui/material';
import PrintIcon from '@mui/icons-material/Print';
import DownloadIcon from '@mui/icons-material/Download';
// eslint-disable-next-line import/no-unresolved
import StageCard from 'components/cards/StageCard';
import useExcelExport from '../../../hooks/useExcelExport.ts';

const useStyles = makeStyles(theme => ({
  '@media print': {
    header: {
      display: 'none !important',
    },
    stack: {
      display: 'block !important',
      '& > *': {
        breakInside: 'avoid',
      },
    },
  },
  stages: {
    width: '100%',
    margin: '0',
    paddingBottom: 66,
    padding: '10px 2em',
    [theme.breakpoints.down('md')]: {
      padding: '0',
    },
  },
  header: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
  },
  title: {
    color: '#0b8253',
    fontSize: '2.3em',
    display: 'flex',
    flex: 1,
  },
  date: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    color: '#AEAEAE',
  },
}));

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const Stages = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const exportData = useExcelExport();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));
  // eslint-disable-next-line react/prop-types
  const { data } = props;

  const compare = useCallback((a, b) => {
    return a.startedAt.localeCompare(b.startedAt);
  }, []);

  const stages = useMemo(() => {
    // eslint-disable-next-line react/prop-types
    return data && data.stages.slice().sort(compare);
  }, [data]);

  const handleClickPrint = useCallback(() => {
    window.print();
  }, []);

  const handleClickExport = useCallback(() => {
    const stagesToExport = stages
      .map(stage => ({
        ...stage,
        url: `${process.env.NEXT_PUBLIC_BASE_URL}/etage/${stage.nae}`,
      }));

    exportData({
      data: stagesToExport,
      columns: ['id', 'name', 'address', 'city', 'shortDescription', 'url'],
      columnLabels: ['ID', 'Nom', 'Adresse', 'Ville', 'Description', 'URL'],
      columnOptions: [{ wch: 4 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 60 }, { wch: 50 }],
      sheetName: 'étapes',
      fileName: 'étapes',
    });
  }, [stages]);

    // Refs for each stage card
    const stageRefs = useRef([]);

    useEffect(() => {
      if (stages.length > 0) {
        // Find the stage with the nearest endedAt from now
        const now = new Date();
        let nearestStageIndex = -1;
        let nearestTimeDifference = Infinity;
  
        stages.forEach((stage, index) => {
          const endedAt = new Date(parseInt(stage.endedAt, 10));
          const timeDifference = Math.abs(endedAt - now);
  
          if (timeDifference < nearestTimeDifference) {
            nearestTimeDifference = timeDifference;
            nearestStageIndex = index;
          }
        });
  
        if (nearestStageIndex !== -1 && stageRefs.current[nearestStageIndex]) {
          // Scroll to the nearest stage card
          stageRefs.current[nearestStageIndex].scrollIntoView({
            behavior: 'smooth',
            block: 'start',
          });
        }
      }
    }, [stages]);
  
  return (
    <div className={classes.stages}>
      <div className={classes.header}>
        {/*
          matches && (
            <div>
              <Tooltip title="Imprimer">
                <IconButton onClick={handleClickPrint} size="large">
                  <PrintIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Exporter">
                <IconButton onClick={handleClickExport} size="large">
                  <DownloadIcon />
                </IconButton>
              </Tooltip>
            </div>
          )
          */}
      </div>
      <Stack spacing={matches ? 2 : 1} className={classes.stack}>
        {stages.map((stage, index) => (
          <div key={stage.id} ref={el => stageRefs.current[index] = el}>
            <StageCard stage={stage} />
          </div>
        ))}
      </Stack>
    </div>
  );
};

export default Stages;
