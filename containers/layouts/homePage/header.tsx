import { Container, Typography } from '@mui/material';
import React from 'react';
import Image from 'next/image';
import { styled } from '@mui/material/styles';

const HeaderContainer = styled(Container)(({ theme }) => ({
  backgroundImage: "url('/image/2024-06-02-Nantes-Adrien-Marie-(Crédit)-reflex-mediactiviste-9262.jpg')",
  backgroundSize: 'cover',
  backgroundBlendMode: 'multiply',
  textAlign: 'center',
  backgroundColor: 'rgba(0,8,58,0.65) !important',
  maxWidth: 'inherit!important',

  [theme.breakpoints.up('md')]: {
    padding: '30px 0 0 0',
  },
  [theme.breakpoints.down('md')]: {
    padding: '5em 0 0 0',
  },
  backgroundPositionY: 'center',
  backgroundPositionX: 'center',
  ':after': {
    content: '""',
    display: 'block',
    backgroundImage:
      'url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0xMjgwIDMuNEMxMDUwLjU5IDE4IDEwMTkuNCA4NC44OSA3MzQuNDIgODQuODljLTMyMCAwLTMyMC04NC4zLTY0MC04NC4zQzU5LjQuNTkgMjguMiAxLjYgMCAzLjRWMTQwaDEyODB6IiBmaWxsLW9wYWNpdHk9Ii4zIi8+PHBhdGggZD0iTTAgMjQuMzFjNDMuNDYtNS42OSA5NC41Ni05LjI1IDE1OC40Mi05LjI1IDMyMCAwIDMyMCA4OS4yNCA2NDAgODkuMjQgMjU2LjEzIDAgMzA3LjI4LTU3LjE2IDQ4MS41OC04MFYxNDBIMHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMTI4MCA1MS43NmMtMjAxIDEyLjQ5LTI0Mi40MyA1My40LTUxMy41OCA1My40LTMyMCAwLTMyMC01Ny02NDAtNTctNDguODUuMDEtOTAuMjEgMS4zNS0xMjYuNDIgMy42VjE0MGgxMjgweiIvPjwvZz48L3N2Zz4=)',
    backgroundSize: '100% 100px',
    bottom: 0,
    height: '100px',
    zIndex: 1,
    transform: 'scale(1,1)',
  },
}));

const BannerTitle = styled(Typography)(({ theme }) => ({
  fontFamily: theme.typography.h1.fontFamily,
  color: theme.palette.primary.contrastText,
  margin: 'auto',
  textWrap: 'balance',
  [theme.breakpoints.up('md')]: {
    fontSize: '40px',
    paddingBottom: '1em',
    maxWidth: theme.breakpoints.values.lg,
  },
  [theme.breakpoints.down('md')]: {
    fontSize: '30px',
    lineHeight: '1.3em',
    fontWeight: '700',
    paddingBottom: '2em',
  },
  marginTop: '1em',
  paddingBttom: '10px',
  fontWeight: '500',
}));

type Props = {
  title: string;
  picture?: string;
};

const Header = ({ title, picture }: Props) => {
  return (
    <HeaderContainer style={{ backgroundImage: picture && `url(${picture})` }}>
      <Image src="/logo.png" alt="Tour Alternatiba 2024" width="202" height="128" />
      <BannerTitle variant="h1">{title}</BannerTitle>
    </HeaderContainer>
  );
};

export default Header;
