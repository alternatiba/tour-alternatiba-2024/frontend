import React from 'react';
import AppLayout from 'containers/layouts/AppLayout';
import { Box, Container,  Typography, Grid } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { Link } from '@mui/material';

const useStyles = makeStyles((theme) => ({
  align: {
    'text-align': 'center',
  },
  main: {
    paddingTop: '5em',

  },
}));
const Charter = () => {
  const styles = useStyles();

  return (
    <AppLayout>
      <>
        <Box>
          <Container component="main" maxWidth="md" className={styles.main}>
            <Typography className={styles.align} variant="h1">Mentions Légales</Typography>
            <br />
            <br />
            <br />
            <Typography variant="body1"> - Nom de l'éditeur : Alternatiba  31 rue Eugene Decout 17000 Rochelle</Typography>
            <Typography variant="body1"> - Hébergeur :  OVH</Typography>
            <Typography variant="body1"> - gestion des données personnelles: </Typography>
            <Typography variant="body1">Conformément à l'article 34 de la loi Informatique et Liberté du 6 janvier 1978, vous disposez d'un droit d'accès, de modification, de rectification et de suppression des données vous concernant. Pour l'exercer, adressez-vous à dpo@alternatiba.eu </Typography>
            <Typography variant="body1"> - Finalités de traitement </Typography>
            <Typography variant="body1"> - durée de conservation des données </Typography>
            <Typography variant="body1"> - suppression automatique des info de contact si mail/teléphone plus valides (remplacement dans la fiche étape des coordonnées par ‘à vérifier » puis après une nouvelle année suppression de la fiche.</Typography>
            <br />
            Aucun cookie tiers n&#39;est utilisé pour analyser le comportement individuel ou les données
              personnelles des visiteurs du site. Plus d&#39;informations sur les cookies et les traceurs sur le site de la
              CNIL : <Link href="https://www.cnil.fr/fr/cookies-et-autres-traceurs/regles/cookies/lignes-directrices-modificatives-et-recommandation" target="_blank" color="inherit" >Cookies et autres traceurs : la CNIL publie des lignes directrices modificatives et sa
          recommandation </Link>
            <br />
          </Container>
        </Box>
      </>
    </AppLayout>
  );
};

export default Charter;
