import React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { Link, Theme, useMediaQuery, useTheme } from '@mui/material';
import StyledBoxOnHover from '../animated/StyledBoxOnHover';
import Image from 'next/image';
import Moment from 'react-moment';

const logoWidthSmall = 60;
const logoWidthBig = 120;
const logoHeightSmall = '40';
const logoHeightBig = '80';

const useStyles = makeStyles((theme: Theme) => ({
  card: {
    backgroundColor: 'white',
    [theme.breakpoints.up('sm')]: {
      minHeight: '100px',
    },
    [theme.breakpoints.down('sm')]: {
      minHeight: `${logoHeightBig}px`,
    },
    display: 'flex',
  },
  content: {
    width: '93%',
    height: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      margin: '0 24px',
    },
    [theme.breakpoints.down('sm')]: {
      margin: '0 5px',
    },
  },
  leftColumn: {
    marginRight: '15px',
    [theme.breakpoints.up('sm')]: {
      maxWidth: `${logoWidthBig}px`,
    },
    [theme.breakpoints.down('sm')]: {
      maxWidth: `${logoWidthSmall}px`,
    },
  },
  rightColumn: {
    overflow: 'hidden',
    flexGrow: 1,
    [theme.breakpoints.up('sm')]: {},
    [theme.breakpoints.down('sm')]: {
      display: '-webkit-box',
      WebkitBoxOrient: 'vertical',
      WebkitLineClamp: 3,
      maxWidth: '100%',
    },
  },
  title: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: 2,
    maxWidth: '100%',
  },
}));

const AboutUsCard = (props) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  const logoWidth = isMobile ? logoWidthSmall : logoWidthBig;
  const logoHeight = isMobile ? logoHeightSmall : logoHeightBig;
  const classes = useStyles({ logoWidth });

  return (
    <StyledBoxOnHover className={classes.card}>
      <Link href={props.link} target="_blank" color="inherit" underline="none" width="100%">
        <div className={classes.content}>
          <div className={classes.leftColumn}>
            <Image
              src={props.media.logo}
              width={logoWidth}
              height={logoHeight}
              alt={props.media.name}
              style={{ maxWidth: `${logoWidth}px`, objectFit: 'contain' }}
            />
          </div>
          <div className={classes.rightColumn}>
            <div className={classes.title}>{props.title}</div>
            <Moment locale="fr" format={isMobile ? 'DD/MM/YYYY' : 'dddd DD MMMM YYYY'} unix>
              {props.publishedAt / 1000}
            </Moment>
          </div>
        </div>
      </Link>
    </StyledBoxOnHover>
  );
};

export default AboutUsCard;
