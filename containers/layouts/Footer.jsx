import makeStyles from '@mui/styles/makeStyles';
import { Container, Typography } from '@mui/material';
import Link from 'components/Link';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TwitterIcon from '@mui/icons-material/Twitter';
import TelegramIcon from '@mui/icons-material/Telegram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import { useRouter } from 'next/router';
import XIcon from '@mui/icons-material/X';

const useStyles = makeStyles((theme) => ({
  footer: {
    color: 'white',
    backgroundColor: theme.palette.primary.main,
    border: 'none',
    textAlign: 'center',
    paddingTop: '1em',
    marginTop: 'auto',
    padding: '0!important',
  },
  footerTitle: {
    color: 'white',
    align: 'center',
    fontWeight: 700,

    fontSize: '2.5em',
    marginTop: '1em',
  },
  footerSubTitle: {
    fontWeight: 100,
    fontSize: '1.2em',
  },
  socialNetworks: {
    '& img': {
      width: '15px',
      height: '15px',
    },
    [theme.breakpoints.up('sm')]: {
      padding: '2% 0',
    },
    [theme.breakpoints.down('md')]: {
      padding: '30px 0',
    },
  },
  icons: {
    fill: 'white',
    color: 'white',
    margin: '0.1em 0.3em',
    width: '25px',
    height: '25px',
  },
  iconMastodon: {
    fill: 'white',
    width: '19px!important',
    height: '30px!important',
    marginLeft: '0.8em',
  },
  iconTikTok: {
    fill: 'white',
    width: '22px!important',
    height: '30px!important',
    marginLeft: '0.3em',
    marginRight: '0.3em',
  },
  iconAlternatiba: {
    width: '46px!important',
    height: '30px!important',
    marginLeft: '0.8em',
  },

  links: {
    '& a': {
      color: 'white',
    },
    fontFamily: theme.typography.fontFamily,
    fontWeight: 500,
    fontWeight: 'bold',
    [theme.breakpoints.up('sm')]: {
      padding: '0 0 2% 0',
    },
    [theme.breakpoints.down('md')]: {
      padding: '0 0 30px 0',
    },
  },
  navigation: {
    '& a': {
      color: 'white',
      fontFamily: theme.typography.fontFamily,
    },
    textTransform: 'uppercase',

    fontWeight: 500,
    fontWeight: 'bold',
    [theme.breakpoints.down('md')]: {
      padding: '30px 0',
    },
    [theme.breakpoints.up('sm')]: {
      padding: '2% 0',
    },
    borderBottom: '1px solid',
  },
  activeLink: {
    color: '#F3D53A!important',
  },

  logo: {},
}));

const Footer = () => {
  const classes = useStyles();
  const router = useRouter();
  return (
    <Container className={classes.footer} maxWidth={false}>
      <div className={classes.navigation}>
        <Link href="/">Accueil</Link>
        <span> - </span>
        <Link href="/itineraire">L'itinéraire</Link>
        <span> - </span>
        <Link href="/en-savoir-plus">En savoir plus</Link>
        <span> - </span>
        <Link href="https://don.alternatiba.eu">Faire un don</Link>
        <span> - </span>
        <Link href="/contact">Nous contacter</Link>
        <span> - </span>
        <Link href="/appel">L'appel</Link>
        <span> - </span>
        <Link href="/etapes">Etapes</Link>
        <span> - </span>
        <Link href="/presse">Presse</Link>
        <span> - </span>
        <Link href="/infolettre">Infolettre</Link>
        <span> - </span>
        <Link href="/actualites">Actualités</Link>
      </div>

      <div className={classes.socialNetworks}>
        <a
          href="https://www.instagram.com/alternatiba_/"
          target="_blank"
          rel="noreferrer"
          aria-label="Suivre Alternatiba sur Instagram"
        >
          <InstagramIcon className={classes.icons} fontSize="large" />
        </a>
        <a
          href="https://www.facebook.com/Alternatiba.eu"
          target="_blank"
          rel="noreferrer"
          aria-label="Suivre Alternatiba sur Facebook"
        >
          <FacebookIcon className={classes.icons} fontSize="large" />
        </a>
        <a
          href="https://twitter.com/Alternatiba_"
          target="_blank"
          rel="noreferrer"
          aria-label="Suivre Alternatiba sur Twitter/X"
        >
          <XIcon className={classes.icons} fontSize="large" />
        </a>
        {/*<a
          href="https://alternatiba.eu"
          target="_blank"
          rel="noreferrer"
          aria-label="Site web d'Alternatiba"
        >
           <img src={"/icons/logo.svg"} alt="Alternatiba" className={classes.iconAlternatiba} />
        </a>
  */}
        <a
          href="https://www.tiktok.com/@alternatiba"
          target="_blank"
          rel="noreferrer"
          aria-label="Suivre Alternatiba sur Tiktok"
        >
          <img src={'/icons/tiktok.svg'} alt="Tiktok" className={classes.iconTikTok} />
        </a>
        <a
          href="https://www.youtube.com/channel/UCjjsC-0NuPc8YgqlmcrOMOw"
          target="_blank"
          rel="noreferrer"
          aria-label="Suivre Alternatiba sur YouTube"
        >
          <YouTubeIcon className={classes.icons} fontSize="large" />
        </a>
        <a
          href="https://piaille.fr/@alternatiba"
          target="_blank"
          rel="noreferrer"
          aria-label="Suivre Alternatiba sur Mastodon"
        >
          <img src={'/icons/mastodon.svg'} alt="Mastodon" className={classes.iconMastodon} />
        </a>
        <a
          href="https://t.me/AlternatibaANVCOP21"
          target="_blank"
          rel="noreferrer"
          aria-label="Suivre Alternatiba sur Telegram"
        >
          <TelegramIcon className={classes.icons} fontSize="large" />
        </a>
        <a
          href="https://www.linkedin.com/company/alternatibaeu/"
          target="_blank"
          rel="noreferrer"
          aria-label="Suivre Alternatiba sur LinkedIn"
        >
          <LinkedInIcon className={classes.icons} fontSize="large" />
        </a>
      </div>
      <div className={classes.links}>
        <Link href="https://alternatiba.eu/mentions-legales/" target="_blank">
          Mentions Légales
        </Link>
        <span> - </span>
        <Link href="https://ouaaa-transition.fr/" target="_blank">
          Propulsé par OUAAA!
        </Link>
        <span> - </span>@2024{' '}
        <Link href="https://alternatiba.eu" target="_blank">
          Alternatiba
        </Link>
      </div>
    </Container>
  );
};

export default Footer;
