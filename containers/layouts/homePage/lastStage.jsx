import { Container, Typography, useTheme } from '@mui/material';
import {  makeStyles } from '@mui/styles';
import React, { useEffect, useState } from 'react';
import Slider from 'react-slick/lib';
import { gql, useQuery } from '@apollo/client';
import useMediaQuery from '@mui/material/useMediaQuery';
import CardSliderStage from '../../../components/cards/CardSliderStage';
import { withApollo } from '../../../hoc/withApollo';
import Link from '../../../components/Link';

const useStyles = makeStyles((theme) => ({
  cardTitle: {
    color: '#0b8253',
    fontFamily: theme.typography.h5.fontFamily,
    textTransform: 'uppercase',
    fontWeight: '400',
    textAlign: 'center',
    fontSize: '3em',
  },
  align: {
    'text-align': 'center',
  },
  stageContainer: {
    paddingTop: '5em',
    paddingBottom: '5em',
    textAlign: 'center',
  },
  border: {
    width: '3em',
    borderColor: '#0b8253',
    borderBottom: 'solid',
    marginLeft: '48%',
    borderBottomColor: '#0b8253',
    color: '#0b8253',
    height: '1em',
    textAlign: 'center',
  },
  buttonGrid: {
    fontSize: '1.5em',
    margin: '2.5em 0 2.5em 0 ',

    borderRadius: '1.5em',
    padding: '0 3em 0 3em',
    height: '2.5em',
    '&:hover': {
      cursor: 'pointer',
      backgroundImage: "url('/arrow.svg')",
      border: 'none',
      color: 'white',
      'background-color': '#0b8253',
    },
    color: '#0b8253',
    'background-color': 'white',
    border: '2px solid #0b8253',
    backgroundImage: "url('/arrow-hover.svg')",

    backgroundRepeat: 'no-repeat',
    'background-position-x': '5px',
    'background-position-y': '1px',
    'background-size': '12%',
  },
  articleCarroussel: {
    paddingTop: '2em',
  },
  buttonArticle: {
    paddingTop: '1em',
    paddingBottom: '1em',
  },
}));
const LastStage = (props) => {
  const GET_STAGES = gql`
    query stages($limit: Int, $sort: String, $way: String,$isValidated: Boolean) {
      stages(limit: $limit, sort: $sort, way: $way, isValidated: $isValidated) {
        id
        name
        address
        lat
        lng
        entries {
          label
        }
        pictures {
          id
          label
          originalPicturePath
          originalPictureFilename
          position
        }
      }
    }
  `;


  const [stageToRender, setStageToRender] = useState(null);

  const {
    data: stageData,
    loading: loadingStage,
    error: errorStage,
  } = useQuery(GET_STAGES, {
    variables: {
      isValidated: true,
      limit: 4,
      sort: 'createdAt',
      way: 'DESC',

    },
  });

  useEffect(() => {
    setStageToRender({
      stageData,
    });
  }, [stageData]);

  function SampleNextArrow(props) {
    // eslint-disable-next-line react/prop-types
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: 'block' }}
        onClick={onClick}
      />
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: 'block' }}
        onClick={onClick}
      />
    );
  }
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));
  const styles = useStyles();
  const maxImageDisplay = !mobile ? 5 : 1
  const settings = {
    dots: true,
    infinite: true,
    slidesToShow:
      stageToRender?.stageData && stageToRender.stageData.stages.length > maxImageDisplay
        ? maxImageDisplay
        : stageToRender?.stageData && stageToRender.stageData.stages.length,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 2000,
    //  pauseOnHover: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  return (
    <Container className={[styles.stageContainer]} id={props.id} >
      <Typography variant="h2" className={[styles.cardTitle]}>
        LES ACTEURS RÉCEMMENTS AJOUTÉS
      </Typography>
      <div className={[styles.border]} />
      <Slider {...settings} className={[styles.articleCarroussel]}>
        {stageToRender?.stageData &&
          stageToRender.stageData.stages.map((stage) => {
            return <CardSliderStage key={stage.id} stage={stage} />;
          })}
      </Slider>
      <div className={styles.buttonArticle}>
        <Link href="/map">
          <button className={styles.buttonGrid}>VOIR TOUS LES ACTEURS</button>
        </Link>
      </div>
    </Container>
  );
};

export default withApollo()(LastStage);
