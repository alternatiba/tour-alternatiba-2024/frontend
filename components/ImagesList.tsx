import React, { useState } from 'react';
import { Box, Card, CardActionArea, CardMedia, Grid, Modal } from '@mui/material';
import { styled } from '@mui/material/styles';
import Image from 'next/image';

type Props = {
  pictures: { src: string; label?: string }[];
};

const ImagePreview = styled(Box)(({ theme }) => ({
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '80vw',
  height: '80vh',
  bgcolor: theme.palette.background.paper,
  boxShadow: '24px',
}));

const ImagesList = ({ pictures }: Props) => {
  const [previewOpen, setPreviewOpen] = useState<number>();

  return (
    <Grid container spacing={2} mb={2} mt={3} justifyContent="center">
      {pictures.map((picture, i) => (
        <Grid item xs={12} sm={6} md={3}>
          <Card>
            <CardActionArea onClick={() => setPreviewOpen(i)}>
              <CardMedia component="img" image={picture.src} />
            </CardActionArea>
          </Card>
        </Grid>
      ))}
      <Modal
        open={previewOpen !== undefined}
        onClose={() => setPreviewOpen(undefined)}
        aria-labelledby="Prévisualisation"
      >
        <ImagePreview onClick={() => setPreviewOpen(undefined)}>
          <Image
            alt={pictures[previewOpen || 0]?.label || ''}
            src={pictures[previewOpen || 0]?.src}
            fill
            objectFit="contain"
          />
        </ImagePreview>
      </Modal>
    </Grid>
  );
};

export default ImagesList;
