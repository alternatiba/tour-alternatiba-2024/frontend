import React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { Avatar, Link,useMediaQuery } from '@mui/material';
import StyledBoxOnHover from '../animated/StyledBoxOnHover';
import { getImageUrl } from '../../utils/utils';
import Favorite from '../Favorite';
import Image from 'next/image';
import Moment from 'react-moment';
import { useTheme } from '@mui/styles';

const useStyles = makeStyles((theme, props) => ({
  '@media print': {
    card: {
      border: 'solid 1px grey',
    },
    favorite: {
      display: 'none !important',
    },
  },
  card: (props) => ({
    backgroundColor: props.passed ? '	#DCDCDC' : 'white',
    
    [theme.breakpoints.up('sm')]: {
      minHeight: '120px',
    },
    [theme.breakpoints.down('md')]: {
      minHeight: '80px',
    },
    display: 'flex',
  }),
  content: (props) => ({
    width: '93%',
    height: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      margin: '0 24px',
    },
    [theme.breakpoints.down('md')]: {
      margin: '0 5px',
    },
    
  }),
  opacity: (props) => ({
    width: '40px',
    height: '40px',
    position: 'absolute',
    borderRadius: '50%',
    backgroundColor: props.color,
    opacity: '0.3',
  }),
  leftContent: {
    display: 'flex',
    alignItems: 'center',
  },
  image: {
    height: '72px',
    width: '72px',
    [theme.breakpoints.up('sm')]: {
      margin: '0 24px 0 0',
    },
    [theme.breakpoints.down('md')]: {
      margin: '0 5px 0 0',
    },
    '& img': {
      height: '100%',
      width: '100%',
      objectFit: 'contain',
      borderRadius: '50%',
    },
  },
  avatar:{
    backgroundColor: 'white',
  },
  text: {},
  stage: (props) => ({
    textTransform: 'uppercase',
    fontWeight: 'bold',
  }),
  label: {
    color: 'black',
    fontWeight: 'bold',
  },
  location: {
    color: 'black',
    fontWeight: 'bold',
    display: 'flex',
    alignItems: 'center',
    fontSize: '0.8em',
  },
  stageDetails: {
    paddingTop: '5px',
  },
  icon: {
    color: '#bd0b3d',
    width: '20px',
  },
}));

const StageCard = ({ stage }) => {
  const color = stage?.entries?.[0]?.parentEntry?.color || '#AD2740';
  const icon = stage?.entries?.[0]? stage.entries[0].icon : 'fruit';
  const stageName = stage.name;
  const passed = new Date(parseInt(stage.endedAt, 10)) < new Date();
  const classes = useStyles({ color, icon,passed });
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <StyledBoxOnHover className={classes.card}>
      <Link href={`/etape/${stage.name}`} target="_blank" color="inherit" underline="none" width="100%">
        <div className={classes.content}>
          <div className={classes.leftContent}>
            <div className={classes.text}>
              <div className={classes.stage}>Étape à {stageName}</div>
              <Moment
                    locale="fr"
                    format="dddd DD MMMM YYYY"
                    unix
                  >
                    {stage.startedAt / 1000}
                  </Moment>
                  {new Date(stage.endedAt).toDateString() !== new Date(stage.startedAt).toDateString() && (
                    <>
                      <span> au </span>
                      <Moment
                        locale="fr"
                        format="DD MMMM YYYY"
                        unix
                      >
                        {stage.endedAt / 1000}
                      </Moment>
                    </>
                  )}
              <div className={classes.label}>{stage.label}</div>
              {!matches && (
              <div className={classes.stageDetails}>
                <span>
                  {/* @ts-ignore */}
                  {stage.shortDescription}
                </span>
              </div>
              )}
            </div>
          </div>
        </div>
      </Link>
    </StyledBoxOnHover>
  );
};

export default StageCard;
