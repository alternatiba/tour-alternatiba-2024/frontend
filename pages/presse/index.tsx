import React from 'react';
import { Container, Grid, Link, Typography } from '@mui/material';
import Image from 'next/image';
import AppLayout from 'containers/layouts/AppLayout';
import Header from 'containers/layouts/homePage/header';
import ImageContainer, { Caption } from 'components/ImageContainer';

const Press = () => {
  return (
    <AppLayout>
      <Header title="Presse" />

      <Container maxWidth="xl" sx={{ marginTop: { xs: 3, md: 6 }, marginBottom: 6 }}>
        <Grid container spacing={6} alignItems="center" rowSpacing={{ xs: 8, md: 16 }}>
          <Grid item xs={12} md={6}>
            <Typography variant="greenTitle">Pour nous contacter</Typography>
            <Typography variant="paragraph">
              Email : <Link href="mailto:presse@alternatiba.eu">presse@alternatiba.eu</Link>
            </Typography>
            <Typography variant="paragraph">Tel : +33 (0)9 72 27 85 95</Typography>

            <Typography variant="greenTitle">S’informer sur le Tour Alternatiba 2024</Typography>
            <Typography variant="paragraph">
              <Link
                target="_blank"
                href="https://kdrive.infomaniak.com/app/share/115077/bc013196-2961-42a2-9b64-acbd7d98046f/preview/pdf/32897"
              >
                Consulter le dossier de presse
              </Link>
            </Typography>
            <Typography variant="paragraph">
              <Link
                target="_blank"
                href="https://kdrive.infomaniak.com/app/share/115077/e00984a9-3b53-4e83-88c8-df1384c36f00/files/32793"
              >
                Visualiser des photos
              </Link>{' '}
              (crédits dans le titre des fichiers)
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <ImageContainer sx={{ width: { xs: '90%', md: '60%' }, aspectRatio: '800/600', margin: 'auto' }}>
              <Image
                src="/image/presse.jpg"
                alt="Interview à la Marche des portraits, Bayonne le 25 août 2019 – photo Michael Augustin"
                fill
              />
              <Caption>Interview à la Marche des portraits, Bayonne le 25 août 2019 – photo Michael Augustin</Caption>
            </ImageContainer>
          </Grid>
        </Grid>
      </Container>
    </AppLayout>
  );
};

export default Press;
