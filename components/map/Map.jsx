import React, { useRef, useState, useEffect } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import {
  MapContainer, TileLayer, Marker, Popup, Tooltip, ZoomControl,Polygon, useMapEvent
} from 'react-leaflet';
import L from 'leaflet';
import 'leaflet.markercluster';

import 'leaflet/dist/leaflet.css';
import { useMap } from "react-leaflet";
import { GestureHandling } from "leaflet-gesture-handling";
import "leaflet-gesture-handling/dist/leaflet-gesture-handling.css";

L.Icon.Default.mergeOptions({
  iconUrl: null,
});



const useStyles = makeStyles((theme) => ({
  polygon:{
    cursor: 'grab',
  }
}));
const MapControler = () => {
  const map = useMap();

  useEffect(() => {
    map.addHandler("gestureHandling", GestureHandling);
    // @ts-expect-error typescript does not see additional handler here
    map.gestureHandling.enable();
  }, [map]);

  return null;
}


const Map = (props) => {
  const mapRef = useRef();
  const styles = useStyles();
  const [map, setMap] = useState(null);
  let { children,position,scrollWheelZoom,classMap,zoom,style } = props;
  //debugger;
  if (typeof position === 'undefined'){
    position = [46.603354, 1.888334];
  }

  useEffect(() => {
    if (map) {
      setInterval(() => {
        try {
          if (map) {
            map.invalidateSize();
          }
        } catch (error) {
        }
      }, 100);
    }
  }, [map]);
  const pathOptions = { color: 'green' ,fill :false}

  const [scrollWheelZoomEnabled, setScrollWheelZoomEnabled] = useState(false);


  const handleMapFocus = () => {
    // setScrollWheelZoomEnabled(true);
  };

  const handleMapClick = () => {
   // debugger;
    setScrollWheelZoomEnabled(true);
  };



  return (
    <MapContainer
      ref={mapRef}
      center={position}
      zoom={zoom!==undefined?zoom:5}
      className={classMap}
      whenCreated={setMap}
      style={style}
    >
      <MapControler/>
      <TileLayer
        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />

      {children}
    </MapContainer>
  );
};

export default Map;
