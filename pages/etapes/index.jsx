import { Stack,Typography ,useTheme,useMediaQuery} from '@mui/material';
import React from 'react';
import AppLayout from 'containers/layouts/AppLayout';
import { withApollo } from 'hoc/withApollo.jsx';
import makeStyles from '@mui/styles/makeStyles';
import Grid from '@mui/material/Grid';
import Head from 'next/head';
import Container from '@mui/material/Container';
import StageCard from '../../components/cards/StageCard';


const GET_STAGES = `
  query stages($entries: [[String]], $search: String,$postCode: String,$favoritesForUser: String,$isValidated: Boolean,$stagesCollective: [[String]]) {
    stages(entries: $entries, search: $search,postCode: $postCode,favoritesForUser: $favoritesForUser,isValidated: $isValidated,stagesCollective:$stagesCollective) {
      id
      name
      address
      city
      shortDescription
      lat
      lng
      startedAt
      endedAt
      entries {
        label
        icon
        color
        description
        parentEntry {
          code
          label
          color
        }
      }
      favorites{
        id
      }
      pictures {
        id
        label
        originalPicturePath
        originalPictureFilename
        position
        logo
      }
    }
  }
`;

const useStyles = makeStyles(theme => ({
  stack: {
    display: 'block !important',
    '& > *': {
      breakInside: 'avoid',
    },
  },
}));
const AnnuairePage = ({ initialData }) => {
  const { data } = initialData;
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));
  const classes = useStyles();
  return (
    <AppLayout>
      <Head>
        <title>
          Étapes
        </title>
      </Head>
      <Container sx={{
        backgroundColor: '#F6F6F6',
      }}
      >
        <Container maxWidth="md">
          <Typography variant="h1" pt={4}>Etapes</Typography>
           <br/>
          {(data?.stages?.map((stage) => {
            <div key={stage.id}>
              <StageCard key={stage.id} stage={stage} />
            </div>
          }))}
            <Stack spacing={matches?2:1} className={classes.stack}>
            {initialData && (data?.stages?.sort((a, b) => (a.startedAt-b.startedAt)).map((stage) => {
              return <StageCard key={stage.id} stage={stage} />;
            }))}
            </Stack>
        </Container>
      </Container>
    </AppLayout>
  );
};

export default withApollo()(AnnuairePage);

export async function getServerSideProps(ctxt) {
  const res = await fetch(process.env.NEXT_PUBLIC_API_URI, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' }, 
    body: JSON.stringify({
      operationName: 'stages',
      query: GET_STAGES,
    }),
  });
  const initialData = await res.json();
  console.log(initialData);

  if (initialData.errors) {
    console.error(
      `Error fetching stages, error message : ${initialData.errors[0].message}`,
    );
  }

  return {
    props: {
      initialData,
    },
  };
}
