import { Grid, Typography } from '@mui/material';
import ActionCard from './ActionCard';

const ActionCards = () => {
  return (
    <Grid item xs={12}>
      <Grid container spacing={6} alignItems="center">
        <Grid item xs={12}>
          <Typography variant="greenTitle" textAlign="center" sx={{ fontSize: { xs: '30px', md: '40px' } }}>
            Nous avons besoin de toi !
          </Typography>
        </Grid>

        {/* <Grid item xs={12} md={6} lg={3}>
          <ActionCard
            imageSrc="/image/home/2024-07-18-Clamecy-Aurélie-Belahsen-Crédit-Alternatiba-ANV-COP21-vélorution.jpg"
            alt="Étape de Clamecy – Tour Alternatiba 2024 – Crédit Alternatiba / ANV-COP21"
            title="Tu souhaites faire partie de l'équipe sur les vélos ?"
            buttonText="Je postule"
            buttonLink="https://docs.google.com/forms/d/e/1FAIpQLSf1mKB0m5ef-BJlhsaD-CS2nzGuk83a2xPgoKCACYX7kW_kRA/viewform"
          />
        </Grid> */}
        {/* <Grid item xs={12} md={6} lg={3}>
          <ActionCard
            imageSrc="/image/home/2024-06-02-Nantes-Adrien-Marie-reflex-mediactiviste-CP-Nantes.jpg"
            alt="Étape de Nantes – Tour Alternatiba 2024 – Crédit Adrien MARIE"
            title="Tu souhaites organiser une des étapes du Tour Alternatiba ?"
            buttonText="J'organise une étape"
            buttonLink="https://bdd.alternatiba.eu/form/qlbbiCDiexrtB2KblgClzfO0vCHvbW-lYogePZB5s3Y"
          />
        </Grid> */}
        <Grid item xs={12} md={6} lg={3}>
          <ActionCard
            imageSrc="/image/home/cite-des-possibles_2024-09-18_16-06-54.jpg"
            alt="Étape de Marseille – Tour Alternatiba 2024 – La Cité des Possibles"
            title="Tu souhaites être bénévole pour l'arrivée à Marseille ?"
            buttonText="Je participe"
            buttonLink="https://noe-app.io/la-cite-des-possibles-arrivee-du-tour-alternatiba/welcome"
          />
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <ActionCard
            imageSrc="/image/home/2024-06-09-Saint-Brieuc-Adrien-MARIE-(crédit)-mediactiviste-reflex-(4-sur-22).jpg"
            alt="Étape de Saint-Brieuc – Tour Alternatiba 2024 – Crédit Adrien MARIE"
            title="Tu souhaites aider au financement du Tour Alternatiba ?"
            buttonText="Je fais un don"
            buttonLink="https://don.alternatiba.eu/"
          />
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <ActionCard
            imageSrc="/image/home/2024-06-20-Dieppe-mediactiviste-centrale-nucléaire-Crédit-Alternatiba-ANV-COP21-Groupe-Sourire.jpg"
            alt="Étape de Dieppe – Tour Alternatiba 2024 – Crédit Alternatiba / ANV-COP21"
            title="Tu souhaites en savoir plus sur le Tour Alternatiba ?"
            buttonText="Je découvre l'appel"
            buttonLink="/appel"
          />
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <ActionCard
            imageSrc="/image/home/2024-06-18-Rouen-Francois-GRINDEL-Credit-Alternatiba-velorution-enfants-portrait-concert-fete-musique-danse-sourire-(85).jpg"
            alt="Étape de Rouen – Tour Alternatiba 2024 – Crédit Alternatiba"
            title="Tu souhaites rejoindre nos projets Alternatiba en Service Civique ?"
            buttonText="Je postule"
            buttonLink="https://www.service-civique.gouv.fr/trouver-ma-mission?keyword=alternatiba&orderby_field=PERTINENCE&orderby_direction=DESC&first=10&page=1&is_region=false&is_department=false&is_country=false"
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ActionCards;