import React, { useRef, useState } from 'react';
import L from 'leaflet';
import { Marker, Tooltip, Popup } from 'react-leaflet';
import { makeStyles } from '@mui/styles';
import { useLeafletContext } from '@react-leaflet/core';

const useStyles = makeStyles((theme) => ({
  tooltip: {
    width: '320px',
  },
}));

const MarkerEventLocation = (props) => {
  const { event } = props;
  const { map } = useLeafletContext();
  const popupRef = useRef();
  const tooltipRef = useRef();


  const [clicked, setClicked] = useState(false);
  const styles = useStyles();
  let icone;
  let color;
  let suitcasePoint;


  icone = '/icons/icon.svg';
    const markerHtmlStyles = `
    fill: #00804D;
    width: 40px;
    height: 40px;
    `;
    suitcasePoint = new L.divIcon({
      iconUrl: icone,
      color,
      fillColor: color,
      iconAnchor: [15, 32], // point of the icon which will correspond to marker's location

      className: styles.tooltip,
      popupAnchor: [18, -30],
      html: `<svg style="${markerHtmlStyles}
      xmlns:dc="http://purl.org/dc/elements/1.1/"
      xmlns:cc="http://creativecommons.org/ns#"
      xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
      xmlns:svg="http://www.w3.org/2000/svg"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
      xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
      width="81.496834mm"
      height="123.74077mm"
      viewBox="0 0 81.496834 123.74077"
      version="1.1"
      id="svg8"
      inkscape:version="0.92.4 (5da689c313, 2019-01-14)"
      sodipodi:docname="icon.svg">
     <defs
        id="defs2" />
     <sodipodi:namedview
        id="base"
        pagecolor="#ffffff"
        bordercolor="#666666"
        borderopacity="1.0"
        inkscape:pageopacity="0.0"
        inkscape:pageshadow="2"
        inkscape:zoom="0.7"
        inkscape:cx="130.92147"
        inkscape:cy="18.253271"
        inkscape:document-units="mm"
        inkscape:current-layer="g4537"
        showgrid="false"
        inkscape:window-width="1920"
        inkscape:window-height="1009"

        inkscape:window-x="-8"
        inkscape:window-y="-8"
        inkscape:window-maximized="1" />
     <metadata
        id="metadata5">
       <rdf:RDF>
         <cc:Work
            rdf:about="">
           <dc:format>image/svg+xml</dc:format>
           <dc:type
              rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
           <dc:title></dc:title>
         </cc:Work>
       </rdf:RDF>
     </metadata>
     <g xmlns="http://www.w3.org/2000/svg" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" inkscape:label="Calque 1" inkscape:groupmode="layer" id="layer1" transform="translate(-1.2067914,-0.78437379)">
     <g id="g4537" transform="matrix(0.26458333,0,0,0.26458333,59.342263,47.254123)">
       <path d="m -64.286279,-174.20513 c -85.060001,0 -154.009771,68.95172 -154.009771,154.011721 0,106.06 154.009771,313.669919 154.009771,313.669919 0,0 154.00976,-207.609919 154.00976,-313.669919 0,-85.060001 -68.94976,-154.011721 -154.00976,-154.011721 z m -0.35743,93.126961 A 64.64286,61.071429 0 0 1 8.3154732e-4,-20.007859 64.64286,61.071429 0 0 1 -64.643709,41.064401 a 64.64286,61.071429 0 0 1 -64.642571,-61.07226 64.64286,61.071429 0 0 1 64.642571,-61.07031 z" id="path4520" inkscape:connector-curvature="0" style="stroke-width:7.55905522;stroke-miterlimit:4;stroke-dasharray:none;stroke:#ffffff;stroke-opacity:0.97457629"/>
       <path style="opacity:1;fill:#ffffff;fill-opacity:1;stroke:#000000;stroke-width:2.59808064;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0.0623622;stroke-opacity:1" d="m 144.36979,215.63112 c -15.03176,-2.67607 -27.11782,-9.03528 -37.13217,-19.53747 -10.842583,-11.37079 -16.405917,-25.18103 -16.405917,-40.72554 0,-19.67099 10.053187,-37.94469 27.129077,-49.31257 11.56397,-7.698444 23.21297,-11.141193 37.52069,-11.088877 17.43109,0.06374 33.22386,6.375307 45.48723,18.178957 9.15324,8.81011 15.32766,20.16211 17.68177,32.50891 0.84036,4.40748 0.84892,15.02728 0.0159,19.67566 -3.37465,18.83017 -15.92715,35.23639 -33.78362,44.15548 -4.18321,2.08947 -10.93377,4.44691 -16.06663,5.61083 -4.83827,1.09711 -19.48721,1.41748 -24.44629,0.53462 z" id="path5092" inkscape:connector-curvature="0" transform="translate(-219.72462,-175.6337)"/>
     </g>
   </g>

   </svg>`,
    });

  return (
    <Marker
      key={`marker-${event.id}`}
      position={[event.lat, event.lng]}
      icon={suitcasePoint}
    >
      <Popup>
        <strong>{event.label}</strong>
        <br />
        { !event.address && event.city && (
          <span>

            {event.city}
          </span>
        )}
        {event.address && event.city && (
          <span>
            {`${event.address}`}
          </span>
        )}
      </Popup>
</Marker>
  );
};

export default MarkerEventLocation;
