import React from 'react';
import { Button, Container, Grid, Link, Typography, styled } from '@mui/material';
import AppLayout from 'containers/layouts/AppLayout';
import Header from 'containers/layouts/homePage/header';

const Highlighted = styled('strong')(({ theme }) => ({
  backgroundColor: theme.palette.tertiary.main,
}));

const Appel = () => {
  return (
    <AppLayout>
      <Header title="Climat : la catastrophe a commencé, organisons la justice et la solidarité !" />

      <Container maxWidth="xl" sx={{ marginTop: { xs: 3, md: 6 }, marginBottom: 6 }}>
        <Grid container spacing={6} justifyContent="center" alignItems="center" rowSpacing={{ xs: 8, md: 16 }}>
          <Grid item xs={12} md={7}>
            <Typography variant="paragraph">
              Malgré des décennies d’alertes, nous sommes désormais entré·es dans l’ère du dérèglement climatique et en
              subissons les premières conséquences : incendies, sécheresses, inondations, tempêtes, sont plus nombreuses
              et destructrices que jamais… Face à cette situation, que peut-on faire ? Les plus riches peuvent
              s’adapter, s’équiper, se déplacer, déménager, réparer les dégâts, acheter au prix fort des ressources
              devenant plus rares et plus chères (énergie, eau, nourriture).{' '}
              <strong>
                Mais la majorité de la population, les classes populaires, les familles et les personnes les plus
                précaires, se retrouvent bien plus démunies face à ces impacts.
              </strong>
            </Typography>
            <Typography variant="paragraph">
              Pourtant, <strong>l’adaptation commence</strong> et la manière dont elle s’organise au quotidien pose les
              bases de la société de demain. <strong>Nous sommes à la croisée des chemins</strong> : construisons-nous
              une société du chacun pour soi, d’inégalités et d’injustices sociales aggravées, ou une société{' '}
              <strong>d’entraide, de partage, de solidarité et de justice sociale ?</strong> Ce choix fondamental se
              joue dès maintenant, par les arbitrages faits sur l’accès à l’eau, à la santé, au logement, à l'éducation
              pour toutes et tous, par les moyens octroyés à la rénovation des bâtiments et aux transports en commun,
              par les politiques agricoles décidées à Paris ou à Bruxelles…
            </Typography>
            <Typography variant="paragraph">
              Si des actions politiques d’ampleur aux niveaux national et international sont indispensables, nous voyons
              bien qu’elles ne se mettent en place ni assez vite, ni avec assez d’ambition,{' '}
              <strong>ni avec assez de justice sociale.</strong> Nous appelons donc à la mobilisation la plus massive
              possible autour des initiatives collectives d’adaptation et de transformation concrète de nos territoires
              pouvant être réalisées directement par les citoyennes et les citoyens, pour davantage de justice et de
              solidarité climatiques.
            </Typography>
            <Typography variant="paragraph">
              <strong>C’est le sens du Tour Alternatiba 2024 auquel nous participons et que nous appelons à rejoindre.</strong>
              Retrouvez-nous sur les nombreuses étapes prévues, qui s'égrènent sur un parcours de plus de 5500 km entre
              le 2 juin à Nantes et le 4 octobre à Marseille. Nous y ferons progresser concrètement les alternatives et
              les résistances portées par la multitude de collectifs, d’associations citoyennes, de coopératives, qui
              irriguent la société autour des valeurs qui nous sont chères : démocratie, justice sociale, féminisme,
              droits humains, anti-racisme et lutte contre les discriminations, accueil des personnes migrantes, paix,
              non-violence, justice internationale.
            </Typography>
            <Typography variant="paragraph">
              Ces quatre mois de mobilisation ininterrompue se traduiront à travers{' '}
              <strong>un large éventail de modes d'action</strong> en fonction des pratiques de nos organisations : mobilisations populaires, construction
              d'alternatives citoyennes, actions spectaculaires, désobéissance civile non-violente assumée à visage
              découvert, plaidoyer, recours en justice…
            </Typography>
            <Typography variant="paragraph">
              Loin des fausses solutions, incertaines, injustes, ou dangereuses, telles que la géo-ingénierie, les OGM,
              les agro-carburants, les méga-bassines, loin des dérives des marchés carbone, de la fuite en avant dans le
              nucléaire, des grands projets d’infrastructures inutiles et imposées, des accords de libre-échange, nous
              devons continuer{' '}
              <strong>
                à développer massivement les milliers d’alternatives concrètes basées sur le respect des équilibres
                écologiques et de la dignité humaine.
              </strong>
            </Typography>
            <Typography variant="paragraph">
              Agriculture biologique et paysanne, consommation responsable, circuits courts, cuisine végétarienne et
              végétalienne, relocalisation de l’économie, partage du travail et des richesses, reconversion sociale et
              écologique de la production, emplois climatiques, finance éthique, défense des biens communs comme l’eau,
              la terre et les forêts, pêche durable, souveraineté alimentaire, solidarité et partage, réparation et
              recyclage, réduction des déchets, mobilités douces, éco-rénovation, lutte contre l’étalement urbain,
              contre l’artificialisation des sols, contre l’invasion publicitaire, préservation du foncier agricole et
              de la biodiversité, sobriété et efficacité énergétique, énergies renouvelables, villes en transition,
              éducation à l’environnement, etc. : non seulement ces alternatives sont{' '}
              <strong>notre meilleure garantie d’adaptation collective à court terme</strong>, mais elles permettent
              également de réduire les émissions de gaz à effet de serre pour{' '}
              <strong>
                limiter l’ampleur de la catastrophe, et éviter de détruire les conditions d’habitabilité de la planète
                de nos enfants.
              </strong>
            </Typography>
            <Typography variant="paragraph">
              <strong>
                Nous appelons nos sympathisant·es, nos adhérent·es, nos allié·es, nos partenaires à faire vivre et à
                faire grandir ces alternatives lors des <Link href="/itineraire">étapes du Tour Alternatiba 2024</Link>
              </strong>
              , qui seront autant d’occasions de coopérer, de se former et{' '}
              <strong>d’organiser des dynamiques d’action citoyenne pour la justice climatique et sociale !</strong>
            </Typography>
          </Grid>
          <Grid item xs={12} md={7}>
            <Typography variant="paragraph">
              Lire l'article de Basta du 22 avril 2024 : <Link href="https://basta.media/climat-la-catastrophe-a-commence-organisons-la-justice-et-la-solidarite-appel-tour-alternatiba-2024-Alain-Damasio">https://basta.media/climat-la-catastrophe-a-commence-organisons-la-justice-et-la-solidarite-appel-tour-alternatiba-2024-Alain-Damasio</Link>
            </Typography>
            <Button
              variant="hoverScaled"
              color="green"
              component={Link}
              href="https://bdd.alternatiba.eu/form/IQNTXrQxTo8jdzWl91dXZWL2LI4tors7UYPbTPtyOH8"
              sx={{ margin: 'auto', marginTop: 4, marginBottom: 4 }}
            >
              Votre organisation souhaite signer l’appel !
            </Button>
            <Typography variant="greenTitle">Premières personnalités signataires de l’appel :</Typography>
            <Typography variant="paragraph">
              <strong>Alain Damasio</strong>, écrivain<br />
              <strong>Cédric Herrou</strong>, paysan solidaire<br />
              <strong>Céline Guivarch</strong>, directrice de recherche au CIRED, membre du Haut Conseil pour le climat et co-autrice du sixième rapport du GIEC<br />
              <strong>Charles Merlin</strong>, “VivreMoinsCon”, créateur de contenu engagé<br />
              <strong>Cyril Pedrosa</strong>, auteur de bande dessinée<br />
              <strong>Kaddour Hadadi</strong>, chanteur de HK et les Saltimbanks<br />
              <strong>Kévin Jean</strong>, enseignant-chercheur en épidémiologie, association Sciences Citoyennes<br />
              <strong>Lucie Pinson</strong>, militante écologiste, prix Goldman de l'environnement, Time 100 climate leaders<br />
              <strong>Maxime Combes</strong>, économiste, membre de l’Aitec<br />
              <strong>Monique Pinçon-Charlot</strong>, sociologue<br />
              <strong>Pierre Rustin</strong>, biologiste et chercheur émérite au CNRS<br />
              <strong>Victoria Berni-André</strong>, militante écologiste<br />
              <strong>Wolfgang Cramer</strong>, directeur de recherche au CNRS-IMBE, co-auteur du sixième rapport du GIEC<br />
            </Typography>
            <Typography variant="greenTitle">Premières organisations signataires de l’appel :</Typography>
            <Typography variant="paragraph">
              <strong>350.org</strong> - Soraya Fettih, chargée de campagnes France<br />
              <strong>Agir pour l’Environnement</strong> - Stephen Kerckhove, directeur général<br />
              <strong>AIRS l'albergerie</strong> - Jean-Michel Viel, mandataire social<br />
              <strong>Alda</strong> - Malika Peyraut, co-présidente<br />
              <strong>Alliance citoyenne - Justice Ensemble</strong> - Amel Doghmane, présidente<br />
              <strong>Alofa Tuvalu</strong> - Gilliane Le Gallic, présidente<br />
              <strong>Alternatiba</strong> - Anne-Sophie Trujillo Gauchez, porte-parole<br />
              <strong>AMAP Velars</strong> - Odile Plantamp, présidente<br />
              <strong>ANV-COP21</strong> - Zoé Pélegry, porte-parole<br />
              <strong>Archipel des Confluences</strong> - Patrick Viveret<br />
              <strong>Assemblée Virtuelle</strong> - Yannick Duthe, développeur<br />
              <strong>Association des Usagers du Vélo, des Voies Vertes et Véloroutes des Vallées de l'Oise</strong> - Thierry Roch, président<br />
              <strong>Association Grands Parents pour le Climat France</strong> - Estelle Le Touze, présidente<br />
              <strong>Association InterVillage pour un Environnement Sain</strong> - Jean-Michel Jedraszak, co-président<br />
              <strong>Association la Solidairerie</strong> - Véronique Journot Lambey, porteuse de projet<br />
              <strong>Association Médiation Nomade</strong> - Yazid Kherfi, fondateur-directeur<br />
              <strong>Attac 44</strong> - Cédric Buron, porte parole<br />
              <strong>ATTAC Cornouaille</strong> - Pierre Crampon, militant<br />
              <strong>Bassines non Merci</strong> - Julien Le Guet, co-porte-parole<br />
              <strong>Bio Equitable en France</strong> - Vincent Rousselet, directeur<br />
              <strong>Biocoop Vienne</strong> - Damien Romatet, gérant<br />
              <strong>Bizi!</strong> - Elise Ayrault, porte-parole<br />
              <strong>Campagne Air-Quotas</strong> - Armel Prieur, président<br />
              <strong>Collectif Marches pour l'alimentation</strong> - Benjamin Ball, porte-parole<br />
              <strong>Collectif StopTotal</strong><br />
              <strong>Combat Monsanto</strong> - Kim Vo Dinh, co-président<br />
              <strong>Concordia</strong> - Claire Iehl, présidente<br />
              <strong>Coopérative Commown</strong> - Delphine Gross, coordinatrice commerciale<br />
              <strong>Coquelicots de Paris</strong> - Jean-Jacques Mabilat, président<br />
              <strong>Douar Bev</strong> - Jean-Yves Jego, porte-parole<br />
              <strong>Eau et Rivières de Bretagne</strong> - Francis Nativel, président<br />
              <strong>Emmaüs France</strong><br />
              <strong>Emmaüs La Roya</strong> - Cédric Herrou, fondateur<br />
              <strong>L'éMoi en Nous</strong> - Emmanuel Marchand, membre<br />
              <strong>Les Enseignant.es pour la Planète</strong> - Pauline Odekerken, secrétaire<br />
              <strong>Extinction Rebellion France</strong><br />
              <strong>Extinction Rébellion Nantes</strong><br />
              <strong>Fédération Artisans du Monde</strong> - Yannick Anvroin, administrateur<br />
              <strong>Fédération des Ami·e·s de l'Erdre</strong> - Charlotte Soleau, présidente<br />
              <strong>France Nature Environnement</strong> - Antoine Gatet, président<br />
              <strong>FSU</strong> - Benoit Teste, Secrétaire général<br />
              <strong>Générations Futures</strong> - Nadine Lauverjat, déléguée générale<br />
              <strong>Geres</strong> - Marie-Noëlle Reboulet, présidente<br />
              <strong>Groupe National de Surveillance des Arbres</strong> - Alexis Boniface, coprésident<br />
              <strong>Greenpeace France</strong> - Jean-François Julliard, directeur général<br />
              <strong>Groupe d’Action Francophone pour l'Environnement - GAFE France</strong> - Jean-Noël Dumont, président<br />
              <strong>Groupe d'Action Francophone pour l'Environnement - GAFE Haïti</strong> - David Tilus, directeur exécutif<br />
              <strong>Groupe des élus Dieppe Écologique et Solidaire</strong> - Frederic Weisz, co-président<br />
              <strong>Groupe Uni-Terre</strong> - Kim Vo Dinh, animateur<br />
              <strong>HelloAsso</strong> - Olivia Lejosne Lilette, responsable des communautés<br />
              <strong>Jour de la Terre</strong> - Clarisse Matta, coordinatrice<br />
              <strong>L'Abeille Cubzaguaise</strong> - Hélène Richet, présidente<br />
              <strong>L'Heureux Cyclage</strong> - Pierre-Eric Letellier, chargé de plaidoyer<br />
              <strong>La Galette, monnaie locale</strong> - Hélène Gerray, secrétaire<br />
              <strong>LDH (Ligue des droits de l'Homme)</strong> - Patrick Baudouin, président<br />
              <strong>Le Relais Jeunes</strong> - Manon Durieux, co-présidente<br />
              <strong>Le Tiers Lien</strong> - Thierry Merle, animateur<br />
              <strong>Le Tournevis</strong> - Fanny Saliou, directrice<br />
              <strong>Les Amis de la Terre</strong> - Khaled Gaiji, président<br />
              <strong>Librairie Utopia</strong> - Elodie Duprat, libraire<br />
              <strong>Lilo.org</strong><br />
              <strong>Locataires Ensemble Grenoble</strong> - Houcine Benmaza, président<br />
              <strong>Low-tech Lab</strong> - Clément Choisne, responsable de l'animation des communautés<br />
              <strong>MarsMob</strong> - Pierre Delareux, porte-parole<br />
              <strong>MIRAMAP (Mouvement Inter-Régional des AMAP)</strong> - Evelyne Boulongne, porte-parole<br />
              <strong>Mouvement pour l’économie solidaire France</strong> - Bruno Lasnier, délégué général<br />
              <strong>Mouvement pour une Alternative Non-violente (MAN)</strong> - Hélène Bourdel, co-porte-parole<br />
              <strong>Mouvement Sol</strong> - Ingrid-Hélène Guet, déléguée générale<br />
              <strong>Mouvement Utopia</strong> - Chantal Richard, membre du bureau national<br />
              <strong>Non-violence XXI</strong> - François Marchand, co-président<br />
              <strong>Notre Affaire à Tous</strong> - Jérémie Suissa, délégué général<br />
              <strong>On est prêt</strong> - Magali Payen, fondatrice<br />
              <strong>Oui au train de nuit</strong> - Nicolas Forien, porte-parole<br />
              <strong>Oxfam France</strong> - Elise Naccarato, responsable plaidoyer climat<br />
              <strong>Plancton du Monde</strong> - Pierre Mollo, président<br />
              <strong>Pour notre santé</strong> - Martin Rieussec-Fournier, président<br />
              <strong>Printemps écologique</strong> - Anne Le Corre, co-fondatrice & porte-parole<br />
              <strong>Rame pour ta Planète</strong> - François Verdet, porte-parole<br />
              <strong>ReAct Transnational</strong> - Eloïse Maulet, directrice du pôle éco-syndicalisme - agro<br />
              <strong>Reclaim Finance</strong> - Lucie Pinson, fondatrice et directrice<br />
              <strong>Réseau Action Climat</strong> - Morgane Créach, directrice générale<br />
              <strong>Réseau AMAP Auvergne-Rhône-Alpes</strong> - Auriane Legendre, chargée de communication<br />
              <strong>Réseau National des Ressourceries et Recycleries</strong> - Sébastien Pichot, co-président<br />
              <strong>Réseau régional des AMAP de Bourgogne Franche-Comté</strong> - Sébastien Barbati, administrateur et représentant du réseau<br />
              <strong>Résistance à l'Agression Publicitaire</strong> - Marie Cousin, co-présidente<br />
              <strong>Rester sur Terre</strong> - Charlène Fleury, coordinatrice du réseau<br />
              <strong>Riposte Alimentaire</strong> - Océane, chargée relations extérieures<br />
              <strong>Scientifiques en rébellion</strong> - Julian Carrey<br />
              <strong>SNETAP-FSU (Syndicat National de l'Enseignement Agricole Technique Agricole Public - Fédération Syndicale Unitaire)</strong> - Frédéric Chassagnette, co-secrétaire général<br />
              <strong>SOS MCS</strong> - Pascale Poupin, présidente<br />
              <strong>SUD Education Aveyron</strong><br />
              <strong>TelesCoop</strong><br />
              <strong>Terre&Humanisme</strong> - Françoise Vernet, présidente<br />
              <strong>Terre de Liens Languedoc Roussillon</strong> - Pauline Avila, animatrice territoriale<br />
              <strong>Transition.lu</strong> - Xavier Turquin, fondateur et gérant<br />
              <strong>Union syndicale Solidaires</strong> - Didier Aubé, secrétaire national<br />
              <strong>Unmondemeilleur.info</strong> - Valérie Le Nigen, présidente<br />
              <strong>WWOOF France</strong> - Cécile Paturel, porte-parole<br />
              <strong>Zero Waste Marseille</strong> - Fiona Cosson, directrice<br />
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </AppLayout>
  );
};

export default Appel;
