# Start in local

For launching in local (Start the Server in local if you want to connect to your local database):

Duplicate .env.dist file, and rename the copy to .env.development

`npm install --legacy-peer-deps` -- to fetch dependancies

`npm run dev` -- launch server on localhost:3000

## Environnement

```
node >= 20
npm >= 10
```

# Build and deploy

Il y a une intégration continue de la branche `develop` sur beta.tour.alternatiba.eu. Voilà comment déployer une nouvelle version sur tour.alternatiba.eu (prod).

Fusionner la branche `develop` dans la branche `main` :

    git checkout develop
    git pull origin develop
    git checkout main
    git pull origin main
    git merge develop
    git push origin main

Se connecter au serveur de recette :

    ssh debian@162.19.24.17

Récupérer le code source :

    cd /home/debian/docker/touralternatiba/source/prod/frontend/
    git pull origin main

Construire l'image Docker (cela prend un petit moment) :

    docker build --cpu-shares=300 --memory=5096m -t tour-alternatiba-frontend:prod-X.Y.Z .

Incrémenter le numéro de version selon la modification qui est faite. Pour un hotfix, incrémenter Z.

NB : pour connaître le numéro de version actuel, regarder l'image qui est en fonctionnement sur `nausicaa`.

Créer une archive contenant l'image :

    cd ..
    docker save --output tour-alternatiba-frontend_image.tar tour-alternatiba-frontend:prod-X.Y.Z

Transférer l'archive sur le serveur de prod (`nausicaa`):
    
    scp tour-alternatiba-frontend_image.tar yoan@nausicaa.alternatiba.eu:/home/docker/tour

Se connecter au serveur de prod :

    ssh yoan@nausicaa.alternatiba.eu

Charger l'image Docker :

    cd /home/docker/tour
    docker load < tour-alternatiba-frontend_image.tar

Mettre à jour le numéro de version de l'image dans le `docker-compose.yml` :

    nano docker-compose.yml

dans le service `tour-alternatiba-frontend`, changer le numéro de l'image : `tour-alternatiba-frontend:prod-X.Y.Z  # X.Y.Z_old`

Lancer le nouveau conteneur Docker :

    docker compose up -d

Vérifier que tout fonctionne correctement en se rendant sur https://tour.alternatiba.eu

Supprimer l'archive :

    rm tour-alternatiba-frontend_image.tar

Supprimer l'ancienne image :

    docker images # lister les images existantes et repérer l'ID de l'image à supprimer
    docker rmi IMAGE_ID

