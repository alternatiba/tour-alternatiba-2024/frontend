import React from 'react';
import AppLayout from 'containers/layouts/AppLayout';
import Header from 'containers/layouts/homePage/header';
import Newsletter from 'containers/layouts/Newsletter';

const Infolettre = () => {
  return (
    <AppLayout>
      <Header title="Inscris-toi aux infolettres" />
      <Newsletter />
    </AppLayout>
  );
};

export default Infolettre;
