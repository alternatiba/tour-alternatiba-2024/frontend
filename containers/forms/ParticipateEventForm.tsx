import { useMutation } from '@apollo/client';
import {
  Container,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  TextField,
} from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import ClassicButton from 'components/buttons/ClassicButton';
import FormController, { RenderCallback, ValidationRules, ValidationRuleType } from 'components/controllers/FormController';
import Checkbox from '@mui/material/Checkbox';
import gql from 'graphql-tag';
import { withApollo } from 'hoc/withApollo';
import { useSnackbar } from 'notistack';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import {
  ChangeEvent, useEffect, useState,
} from 'react';
import withDndProvider from 'hoc/withDnDProvider';

const PROPOSE_STAGEFORM = gql`
  mutation addParticipateEvent($formValues: ParticipateEventInfos!, $eventId: Int!) {
    addParticipateEvent(participateEventInfos: $formValues,eventId: $eventId)
  }
`;

const useStyles = makeStyles((theme) => ({
  formContainer: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(8),
  },
  field: {
    marginBottom: '25px!important',
  },
  radioGroup: {
    marginBottom: theme.spacing(3),
    justifyContent: 'center',
  },
}));

type FormItemProps = {
  label: string;
  inputName: string;
  formChangeHandler: (event: ChangeEvent) => void;
  value: string;
  required: boolean;
  errorBool: boolean;
  errorText: string;
  helperText?: string;
  multiline?: boolean;
  fullWidth?: boolean;
  minRows?: number;
  maxRows?: number;
};

const FormItem = (props: FormItemProps) => {
  const styles = useStyles();
  const {
    label,
    inputName,
    formChangeHandler,
    value,
    required,
    errorBool,
    errorText,
    helperText,
    multiline,
    fullWidth,
    minRows,
    maxRows,
  } = props;
  return (
    <TextField
      variant="outlined"
      value={value}
      label={label}
      name={inputName}
      onChange={formChangeHandler}
      fullWidth={fullWidth}
      required={required}
      error={errorBool}
      helperText={errorBool ? errorText : helperText}
      multiline={multiline}
      minRows={minRows}
      maxRows={maxRows}
      className={styles.field}
    />
  );
};

const ParticipateEventForm= (props) => {
  const {
    event,
    onClose
  } = props;

  const styles = useStyles();
  const [messageSent, setMessageSent] = useState(false);
  const [lastStageNameSent, setLastStageNameSent] = useState('');
  const [category, setCategory] = useState('message');



  const Form: RenderCallback = (props) => {
    const { formChangeHandler, formValues, validationResult } = props;
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();


    const [
      addParticipateEvent,
      { data: addParticipateEventData, error: addParticipateEventError, loading: addParticipateEventLoading },
    ] = useMutation(PROPOSE_STAGEFORM);

    const inputs = [

    ];


     inputs.push({
      label: 'Prénom',
      name: 'firstName',
      errorText: 'Prénom requis.',
      required: true,
      fullWidth: true,
    });

    inputs.push({
      label: 'Nom',
      name: 'lastName',
      required: true,
      errorText: 'Nom requis.',
      fullWidth: true,
    });


    inputs.push({
      label: "Adresse mail",
      name: 'email',
      required: true,
      errorText: 'Email requis.',
      fullWidth: true,
    });
    inputs.push({
      label: `Numéro de téléphone ${event?.category === 'dcnv' ? '' : '(facultatif)'}`,
      name: 'phone',
      required: event?.category === 'dcnv',
      fullWidth: true,
    });
    if(event?.category === 'dcnv'){
      inputs.push({
        label: "De quel territoire viens-tu ? ",
        name: 'territory',
        required: true,
        fullWidth: true,
      });

      inputs.push({
        label: "Es-tu déjà impliqué·e dans une ou plusieurs association(s) ? *",
        name: 'association',

        required: true,
        fullWidth: true,
        select: true,
        options: [
          { value: 'true', label: "Oui" },
          { value: 'false', label: "Non" },
        ],
      });

      inputs.push({
        label: "Si oui, laquelle ?",
        name: 'associationName',
        required: false,
        fullWidth: true,
      });

      inputs.push({
        label: "As-tu déjà de l'expérience en action non-violente ? *",
        name: 'experience',
        required: true,
        fullWidth: true,
        select: true,
        options: [
          { value: 'none', label: "Non, je n'ai encore jamais participé à des actions non-violentes !" },
          { value: 'small', label: "Oui, j'ai déjà participé à des petites actions, sans risques physiques ou juridiques" },
          { value: 'risk', label: "Oui, j'ai déjà participé à des actions non-violentes avec risques physiques et/ou juridiques" },
          { value: 'coordination', label: "Oui, j'ai déjà coordonné des actions non-violentes" },
        ],
      });

      inputs.push({
        label: "J'accepte d'être tenu·e informé·e des prochaines actions organisées près de mon territoire suite à cette formation (je pourrai me désinscrire à tout moment)",
        name: 'subscribe',
        required: false,
        fullWidth: true,
        component: Checkbox,
      });




    }

    const inputError = (name: string) => {
      return !validationResult?.global && !!validationResult?.result[name] && formValues[name] !== undefined;
    };

    const submitContactForm = () => {
      if(event?.category === 'dcnv'){
        formValues.association = formValues.association[0] === "true" ? true : false;
        delete formValues.entriesWithInformation;
        delete formValues.subscribe;
      }
    
      addParticipateEvent({
        variables: {
          formValues: {
            ...formValues,
          },
          eventId: parseInt(event.id),
        },
      });
    };

    useEffect(() => {
      if (!addParticipateEventLoading && addParticipateEventData) {
        enqueueSnackbar('Votre participation a bien été enregistrée.', {
          preventDuplicate: true,
        });
        onClose();
      } else if (addParticipateEventError) {
        enqueueSnackbar(addParticipateEventError.message, {
          preventDuplicate: true,
        });
      }
    }, [addParticipateEventData, addParticipateEventError, addParticipateEventLoading]);

    const getFormInputs = (() => {
      return (
        <div>
          {inputs &&
            inputs.map((input, i) => {
                if (input.component === Checkbox) {
                return (
                  <>
                  <FormControlLabel
                  key={i}
                  control={
                    <Checkbox
                    onChange={formChangeHandler}
                    name={input.name}
                    />
                  }
                  label={input.label}
                  />
                  <br />
                  <br />
                  </>
                );
                } else if (input.select) {
                return (
                  <>
                    <FormControl component="fieldset">
                      <FormLabel id={`${input.name}-label`}>{input.label}</FormLabel>
                      <RadioGroup
                        aria-labelledby={`${input.name}-label`}
                        name={input.name}
                        onChange={formChangeHandler}
                      >
                        {input.options.map((option, index) => (
                          <FormControlLabel
                            key={index}
                            value={option.value}
                            control={<Radio />}
                            label={option.label}
                          />
                        ))}
                      </RadioGroup>
                    </FormControl>
                    <br />
                    <br />
                  </>
                );
                } else {
                return (
                  <FormItem
                  key={i}
                  label={input.label}
                  inputName={input.name}
                  formChangeHandler={formChangeHandler}
                  value={formValues[input.name]}
                  required={input.required}
                  errorBool={inputError(input.name)}
                  errorText={input.errorText}
                  multiline={input.multiline}
                  fullWidth={input.fullWidth}
                  minRows={input.minRows}
                  maxRows={input.maxRows}
                  />
                );
                }
            })}
        </div>
      );
    });

    return (
      <Container maxWidth="md" className={styles.formContainer}>
        { getFormInputs() }
        <ClassicButton
          fullWidth
          variant="contained"
          onClick={submitContactForm}
          disabled={!validationResult?.global}
        >
          {"Participer à l'événénement"}
        </ClassicButton>
      </Container>
    );
  };
  let validationRules: ValidationRules;

    validationRules = {
      firstName: {
        rule: ValidationRuleType.required,
      },
      lastName: {
        rule: ValidationRuleType.required,
      },
      email: {
        rule: ValidationRuleType.required && ValidationRuleType.email,
      },
    };

    if (event?.category === 'dcnv') {
      validationRules.phone = {
        rule: ValidationRuleType.required,
      };
      validationRules.subscribe = {
        rule: ValidationRuleType.required,
      };
      validationRules.experience = {
        rule: ValidationRuleType.required,
      };
      validationRules.territory = {
        rule: ValidationRuleType.required,
      };
      validationRules.association = {
        rule: ValidationRuleType.required,
      };
    }

  return (
    <FormController
      render={Form}
      validationRules={validationRules}
    />
  );
};

export default withDndProvider(withApollo()(ParticipateEventForm));
