import { Container, Typography, useTheme, Grid, Stack, Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import React, { useMemo, useRef } from 'react';
import useMediaQuery from '@mui/material/useMediaQuery';
import Link from '../../../components/Link';
import Image from 'next/image';
import dynamic from 'next/dynamic';
import { gql, useQuery } from '@apollo/client';
import ActionCard from 'components/cards/ActionCard';
import ImageContainer, { Caption } from 'components/ImageContainer';
import ActionCards from 'components/cards/ActionCards';

const MapWithNoSSR = dynamic(() => import('../../../components/map/Map'), {
  ssr: false,
});

const Polyline = dynamic(() => import('react-leaflet').then((module) => module.Polyline), { ssr: false });

const MarkerWithNoSSR = dynamic(() => import('../../../components/map/StageMarker'), {
  ssr: false,
});

const useStyles = makeStyles((theme) => ({
  mapContainer: {
    zIndex: '0',
    [theme.breakpoints.up('md')]: {
      height: '500px !important',
    },
    [theme.breakpoints.down('md')]: {
      height: '400px !important',
      width: '100% !important',
    },
  },
}));

const GET_STAGES = gql`
  query stages(
    $entries: [[String]]
    $search: String
    $postCode: String
    $favoritesForUser: String
    $isValidated: Boolean
    $stagesCollective: [[String]]
  ) {
    stages(
      entries: $entries
      search: $search
      postCode: $postCode
      favoritesForUser: $favoritesForUser
      isValidated: $isValidated
      stagesCollective: $stagesCollective
    ) {
      id
      name
      address
      city
      shortDescription
      startedAt
      endedAt
      lat
      lng
      pictures {
        id
        label
        originalPicturePath
        originalPictureFilename
        position
        logo
      }
    }
  }
`;

const PresentationSection = (props) => {
  const styles = useStyles();
  const mapRef = useRef();
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('md'));
  let stagesWithLocation;
  const isSSR = () => typeof window !== 'undefined';

  if (isSSR) {
    const { data, refetch } = useQuery(GET_STAGES, {
      variables: {},
    });
    stagesWithLocation = useMemo(() => {
      return (data?.stages || []).filter((stage) => stage.lat && stage.lng).sort((a, b) => a.startedAt - b.startedAt);
    }, [data]);
  }

  return (
    <Container maxWidth="xl" id={props.id} sx={{ marginTop: { xs: 3, md: 6 }, marginBottom: 6 }}>
      <Grid container spacing={6} alignItems="center" sx={{ marginBottom: 6 }}>
        <Grid item xs={12} md={6}>
          <Typography variant="greenTitle">
            Le Tour Alternatiba arrive à Marseille les 4, 5 et 6 octobre !
          </Typography>
          <Typography variant="paragraph">
            <strong>Le Tour Alternatiba 2024</strong>, c’est une immense mobilisation citoyenne pour transformer
            ensemble les territoires de manière concrète, soutenable et désirable, face aux conséquences du dérèglement
            climatique que nous subissons déjà. À bord de nos vélos 3 et 4 places, nous avons pris le départ à Nantes
            le 2 juin, et parcourons les territoires, nous arrêtant dans les villes, les quartiers, les villages, les
            lieux-dits qui nous ouvrent leurs portes. <strong><a href="https://alternatibamarseille.org/programmation">
            La grande arrivée du Tour Alternatiba</a></strong> aura lieu à Marseille les 4, 5 et 6 octobre sur le bas
            de la Canebière, la place du Général de Gaulle et le square Léon Blum pour un weekend de mobilisations et de
            fête avec des concerts, des tables rondes, un village des alternatives, une vélorution et plein d'autres
            surprises !
          </Typography>
          <Stack direction={{ xs: 'column', md: 'row' }} spacing={3}>
            <Button component={Link} href="/itineraire" variant="hoverScaled" color="green">
              Découvrir la carte
            </Button>
            <Button component={Link} href="/en-savoir-plus" variant="hoverScaled" color="greenInverted">
              Nous Découvrir
            </Button>
          </Stack>
        </Grid>
        {!mobile && (
          <Grid item md={6}>
            <ImageContainer sx={{ width: { xs: '90%', md: '70%' }, aspectRatio: '1080/1080', margin: 'auto' }}>
              <Image src="/image/home/tour-alternatiba-marseille-4-5-6-octobre-2024.png" alt="La Cité des Possibles à Marseille les 4, 5 et 6 octobre – Tour Alternatiba 2024" fill />
              {/* <Caption>La Cité des Possibles à Marseille les 4, 5 et 6 octobre – Tour Alternatiba 2024</Caption> */}
            </ImageContainer>
          </Grid>
        )}

        <Grid item xs={12}>
          <MapWithNoSSR id="map" scrollWheelZoom={false} classMap={styles.mapContainer}>
            {stagesWithLocation && stagesWithLocation.map((stage) => {
              const nextStage = stagesWithLocation?.filter((s) => new Date(parseInt(s.endedAt, 10)) > new Date());
                const isNextStage = nextStage.length !== 0 && stage.id === nextStage[0]?.id;
              return <MarkerWithNoSSR stage={stage} isNextStage={isNextStage} />;
            })}
        {/**<Polyline positions={stagesWithLocation?.filter((stage) => new Date(parseInt(stage.endedAt, 10)) > new Date()).map((stage) => [stage.lat, stage.lng])} color="#0b8253" weight={3}></Polyline> **/}
            <Polyline positions={stagesWithLocation?.map((stage) => [stage.lat, stage.lng])} color="#0b8253" weight={3}></Polyline>
          </MapWithNoSSR>
        </Grid>

        <ActionCards />
      </Grid>
    </Container>
  );
};

export default PresentationSection;
